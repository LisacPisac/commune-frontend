[![pipeline status](https://gitlab.com/LisacPisac/commune-frontend/badges/master/pipeline.svg)](https://gitlab.com/LisacPisac/commune-frontend/-/commits/master)

[![coverage report](https://gitlab.com/LisacPisac/commune-frontend/badges/master/coverage.svg)](https://gitlab.com/LisacPisac/commune-frontend/-/commits/master) 

# Hosted demo

A demo is hosted using GitLab pages and can be accessed via the following link: https://lisacpisac.gitlab.io/commune-frontend/
