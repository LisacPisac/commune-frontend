import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from "./public/page-not-found/page-not-found.component";

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./core-layout/core-layout.module').then(m => m.CoreLayoutModule),
    },
    {
        path: '**',
        component: PageNotFoundComponent,
        data: {title: '404', breadcrumb: '404'},

    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {enableTracing: false, onSameUrlNavigation: "reload"})],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
