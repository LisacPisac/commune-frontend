import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from "rxjs";
import {environment} from "../../../environments/environment";
import {ContactsApiService, PostsApiService, UsersApiService} from "../../shared/api";
import {IndexPostsPayload, Post, User} from "../../shared/models";
import {ProfileSettingsDialogComponent} from "../profile-settings-dialog/profile-settings-dialog.component";
import {AuthUserService, GlobalLoadingService} from '../../shared/services';
import {
    PendingContactsDialogComponent
} from "../../shared/components/dialogs/pending-contacts-dialog/pending-contacts-dialog.component";

@Component({
    selector: 'app-main-profile',
    templateUrl: './main-profile.component.html',
    styleUrls: ['./main-profile.component.scss']
})
export class MainProfileComponent implements OnInit, OnDestroy {

    user: User;
    profilePosts: Post[] = [];
    contacts: User[] = [];

    pendingRequests: number = 0;

    subscriptions: Subscription = new Subscription();

    mediaUrl = environment.media_url;

    private page = 1;
    private pageSize = 10;
    private filters: IndexPostsPayload = {
        page: this.page,
        page_size: this.pageSize
    };

    constructor(
        public dialog: MatDialog,
        public authUser: AuthUserService,
        private postApi: PostsApiService,
        private usersApi: UsersApiService,
        private contactApi: ContactsApiService,
        private loadingService: GlobalLoadingService
    ) {
    }

    ngOnInit(): void {
        this.user = this.authUser.user;
        this.loadPosts();
        this.loadContacts();
        this.getPendingCountForUser();
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    loadPosts() {
        this.loadingService.start();
        this.subscriptions.add(
            this.postApi.getForUser(this.user.id, this.filters).subscribe(
                (result) => {
                    if (result.length > 0) {
                        this.profilePosts = [...this.profilePosts, ...result];
                    } else {
                        this.page--;
                    }
                    this.loadingService.finish();
                },
                (error: HttpErrorResponse) => {
                    this.loadingService.finish();
                }
            )
        );
    }

    loadContacts() {
        this.loadingService.start();
        this.subscriptions.add(
            this.contactApi.getForUser(this.user.id).subscribe(
                (contacts) => {
                    this.contacts = contacts;
                    this.loadingService.finish();
                }
            )
        )
    }

    openSettings() {
        const dialogRef = this.dialog.open(ProfileSettingsDialogComponent, {
            data: {
                user: this.user,
            }
        });

        this.subscriptions.add(dialogRef.afterClosed().subscribe(
            (result) => {
                this.user = this.authUser.user;
            }
        ));
    }

    getPendingCountForUser() {
        this.subscriptions.add(
            this.contactApi.getPendingCountForUser(this.user.id).subscribe(
                (response) => {
                    this.pendingRequests = response;
                }
            )
        );
    }

    openContactRequests() {
        const dialogRef = this.dialog.open(PendingContactsDialogComponent);
    }

    onPostCreated(post: Post) {
        if (post) {
            this.profilePosts.unshift(post);
        }
    }

    loadPage() {
        this.page++;
        this.setFilters();
        this.loadPosts()
    }

    private setFilters() {
        this.filters = {
            page: this.page,
            page_size: this.pageSize
        };
    }
}
