import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileGuard} from '../shared/guards/profile-guard.guard';
import {UserResolver} from '../shared/resolvers/user.resolver';
import {MainProfileComponent} from "./main-profile/main-profile.component";
import {UserProfileComponent} from './user-profile/user-profile.component';

const routes: Routes = [
    {
        path: '',
        component: MainProfileComponent,
    },
    {
        path: ':userId',
        component: UserProfileComponent,
        runGuardsAndResolvers: 'always',
        resolve: { user: UserResolver },
        canActivate: [ProfileGuard]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule {
}
