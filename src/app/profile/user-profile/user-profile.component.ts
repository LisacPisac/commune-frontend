import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PostsApiService} from 'src/app/shared/api/posts-api.service';
import {IndexPostsPayload, Post} from 'src/app/shared/models/Post';
import {User} from 'src/app/shared/models/User';
import {AuthUserService} from 'src/app/shared/services/auth-user.service';
import {GlobalLoadingService} from 'src/app/shared/services/global-loading.service';
import {environment} from 'src/environments/environment';
import {ChatApiService, ContactsApiService} from "../../shared/api";
import {Contact, ContactStatusEnum} from "../../shared/models";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ErrorService} from "../../shared/services";

@Component({
    selector: 'app-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {

    readonly contactStatusConstants = ContactStatusEnum;

    user: User;
    posts: Post[] = [];
    contacts: User[] = [];

    contactStatus = this.contactStatusConstants.NOT_CONTACTS;


    mediaUrl = environment.media_url;

    private subscriptions: Subscription = new Subscription();
    private page = 1;
    private pageSize = 10;
    private filters: IndexPostsPayload = {
        page: this.page,
        page_size: this.pageSize
    };

    constructor(
        public dialog: MatDialog,
        public authUser: AuthUserService,
        private postApi: PostsApiService,
        private contactApi: ContactsApiService,
        private chatApi: ChatApiService,
        private loadingService: GlobalLoadingService,
        private route: ActivatedRoute,
        private router: Router,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.user = this.route.snapshot.data.user;

        this.loadPosts();
        this.loadContacts();
        this.getContactStatus();
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    addContact() {
        this.loadingService.start();
        this.subscriptions.add(this.contactApi.addContact(this.user.id).subscribe(
            (contact: Contact) => {
                if (contact.pending) {
                    this.contactStatus = this.contactStatusConstants.PENDING;
                }
                this.snackBar.open('Contact request sent', 'dismiss', {duration: 5000});
                this.loadingService.finish();
            },
            (error: HttpErrorResponse) => {
                this.loadingService.finish();
                this.snackBar.open(this.errorService.handleErrors(error), 'X');
            }
        ));
    }

    removeContact(isCancel = false) {
        this.loadingService.start();
        this.subscriptions.add(
            this.contactApi.removeContact(this.user.id).subscribe(
                () => {
                    this.contactStatus = this.contactStatusConstants.NOT_CONTACTS;
                    if (isCancel) {
                        this.snackBar.open('Contact request cancelled', 'dismiss', {duration: 5000});
                    } else {
                        this.snackBar.open('Contact removed', 'dismiss', {duration: 5000});
                    }
                    this.loadingService.finish();
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    this.loadingService.finish();
                }
            )
        );
    }

    getContactStatus() {
        this.subscriptions.add(this.contactApi.status(this.authUser.userId, this.user.id).subscribe(
            (status: any) => {
                this.contactStatus = status.status;
            },
            (error: HttpErrorResponse) => {
                this.snackBar.open(this.errorService.handleErrors(error), 'X');
            }
        ));
    }

    startChat() {
        this.subscriptions.add(
            this.chatApi.createChat(this.user.id).subscribe(
                (result) => {
                    this.router.navigate(['chat']);
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            )
        )
    }

    loadPosts() {
        this.loadingService.start();
        this.subscriptions.add(
            this.postApi.getForUser(this.user.id, this.filters).subscribe(
                (result) => {
                    if (result.length > 0) {
                        this.posts = [...this.posts, ...result];
                    } else {
                        this.page--;
                    }
                    this.loadingService.finish();
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    this.loadingService.finish();
                }
            )
        );
    }

    loadContacts() {
        this.loadingService.start();
        this.subscriptions.add(
            this.contactApi.getForUser(this.user.id).subscribe(
                (contacts) => {
                    this.contacts = contacts;
                    this.loadingService.finish();
                }
            )
        )
    }

    loadPage() {
        this.page++;
        this.setFilters();
        this.loadPosts()
    }

    private setFilters() {
        this.filters = {
            page: this.page,
            page_size: this.pageSize
        };
    }
}
