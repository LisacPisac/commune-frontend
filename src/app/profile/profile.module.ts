import {NgModule} from '@angular/core';
import {SharedModule} from "../shared/shared.module";
import {GroupsModule} from './../groups/groups.module';
import {MainProfileComponent} from './main-profile/main-profile.component';
import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileSettingsDialogComponent} from './profile-settings-dialog/profile-settings-dialog.component';
import {UserProfileComponent} from './user-profile/user-profile.component';


@NgModule({
    declarations: [
        MainProfileComponent,
        ProfileSettingsDialogComponent,
        UserProfileComponent
    ],
    imports: [
        SharedModule,
        ProfileRoutingModule,
        GroupsModule
    ],
})
export class ProfileModule {
}
