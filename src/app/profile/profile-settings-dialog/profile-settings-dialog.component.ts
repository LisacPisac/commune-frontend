import {Component, Inject, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivateTfaPayload, TfaSecret, UpdateUserPayload, User} from "../../shared/models";
import {AuthApiService, BillingApiService, UsersApiService} from "../../shared/api";
import {Subscription} from "rxjs";
import {HttpErrorResponse, HttpEventType} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AuthUserService, ErrorService} from "../../shared/services";
import {Router} from "@angular/router";

@Component({
    selector: 'app-profile-settings-dialog',
    templateUrl: './profile-settings-dialog.component.html',
    styleUrls: ['./profile-settings-dialog.component.scss']
})
export class ProfileSettingsDialogComponent implements OnInit, OnDestroy {

    @ViewChild('qrCode') qrCodeImg: HTMLImageElement;

    profileForm!: FormGroup;
    tfaForm?: FormGroup;
    user!: User;
    tfaToggleFc: FormControl;
    loading: boolean = false;

    uploadProgress: number = 0;

    response: any;
    tfaResponse: TfaSecret;

    isConfirmDelete = false;

    private subscriptions = new Subscription();

    constructor(
        public dialogRef: MatDialogRef<ProfileSettingsDialogComponent>,
        public authUser: AuthUserService,
        @Inject(MAT_DIALOG_DATA) public data: { user: User },
        private formBuilder: FormBuilder,
        private authApi: AuthApiService,
        private userApi: UsersApiService,
        private snackbar: MatSnackBar,
        private billingApi: BillingApiService,
        private router: Router,
        private errorService: ErrorService
    ) {
    }

    ngOnInit(): void {
        this.user = this.data.user;
        this.profileForm = this.formBuilder.group({
            email: ['', [Validators.required]],
            username: ['', [Validators.required]],
            summary: [''],
            subtitle: [''],
            avatar: [null],
        });

        this.tfaForm = this.formBuilder.group({
            tfa_code: [''],
        });

        this.tfaToggleFc = new FormControl(this.user.tfa_enabled);
        this.initForm();
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    initForm() {
        this.profileForm.get('email').patchValue(this.user.email);
        this.profileForm.get('username').patchValue(this.user.username);
        this.profileForm.get('subtitle').patchValue(this.user.subtitle);
        this.profileForm.get('summary').patchValue(this.user.summary);
    }

    onSubmit() {
        const payload: UpdateUserPayload = {
            email: this.profileForm.get('email').value,
            username: this.profileForm.get('username').value,
            subtitle: this.profileForm.get('subtitle').value,
            summary: this.profileForm.get('summary').value,
        };

        if (this.profileForm.get('avatar').value) {
            payload.avatar = this.profileForm.get('avatar').value;
            this.resetUpload();
        }

        this.subscriptions.add(
            this.userApi.updateProfile(this.user.id, payload)
                .subscribe(
                    (event) => {
                        if (event.type == HttpEventType.UploadProgress) {
                            this.uploadProgress = Math.round(100 * (event.loaded / event.total));
                        }

                        if (event.type == HttpEventType.Response) {
                            this.snackbar.open('Profile successfully updated.', 'Dismiss');
                            this.authUser.user = event.body.data;
                        }
                    },
                    (error: HttpErrorResponse) => {
                        this.errorService.handleErrors(error, this.profileForm);
                    },
                    () => {

                    }
                )
        );
    }

    onTfaToggle() {
        this.tfaToggleFc.setValue(!this.tfaToggleFc.value);

        if (this.tfaToggleFc.value && !this.user.tfa_enabled) {
            this.loading = true;
            this.subscriptions.add(
                this.authApi.generateTwoFactorSecret().subscribe(
                    (result) => {
                        this.loading = false;
                        this.tfaResponse = result;
                        this.qrCodeImg.src = this.tfaResponse.qr_code;
                    },
                    (error: HttpErrorResponse) => {
                        this.loading = false;
                        this.snackbar.open(this.errorService.handleErrors(error), 'X');
                    }
                )
            )
        } else {
            this.loading = false;
        }
    }

    onSubmitTfa() {
        const payload: ActivateTfaPayload = {
            code: this.tfaForm.get('tfa_code').value,
        }

        this.loading = true;
        this.subscriptions.add(
            this.authApi.activateTfa(payload).subscribe(
                (result) => {
                    this.user = result;
                    this.loading = false;
                },
                (error: HttpErrorResponse) => {
                    this.loading = false;
                }
            )
        )
    }

    disableTfa() {
        this.subscriptions.add(
            this.authApi.deactivateTfa().subscribe(
                (result) => {
                    this.user = result;
                    this.tfaToggleFc.patchValue(false);
                }
            )
        )
    }

    private resetUpload() {
        this.uploadProgress = null;
    }

    buyPremium() {
        this.subscriptions.add(
            this.billingApi.createOrder().subscribe(
                (result) => {
                    window.open(result);
                },
            )
        )
    }

    onDelete() {
        this.isConfirmDelete = true;
    }

    onConfirmDelete() {
        this.subscriptions.add(
            this.userApi.delete(this.authUser.userId).subscribe(
                () => {
                    this.snackbar.open('Your account has been deleted.', 'X', {duration: 4000}),
                        this.router.navigate(['login']);
                },
                (error: HttpErrorResponse) => {
                    this.snackbar.open(this.errorService.handleErrors(error), 'X');
                }
            )
        )
    }
}
