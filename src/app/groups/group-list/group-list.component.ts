import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {GroupsApiService} from 'src/app/shared/api/groups-api.service';
import {Group, IndexGroupsPayload} from 'src/app/shared/models/Group';
import {AuthUserService} from 'src/app/shared/services/auth-user.service';
import {Router} from "@angular/router";

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.scss']
})
export class GroupListComponent implements OnInit {

    groups: Group[];

    private subscription: Subscription;

    private page: number = 1;
    private pageSize: number = 10;

    constructor(
        private authUser: AuthUserService,
        private groupsApi: GroupsApiService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.loadData();
    }

    loadData() {
        const payload: IndexGroupsPayload = {
            page: this.page,
            limit: this.pageSize
        }
        this.subscription = this.groupsApi.index(payload).subscribe(
            (groups) => {
                this.groups = groups;
            },
            (error: HttpErrorResponse) => {
            }
        )
    }

    onGroupCreated(group: Group) {
        if (group) {
            this.router.navigate(['groups', group.id]);
        }
    }
}
