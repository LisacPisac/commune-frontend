import {HttpErrorResponse, HttpEvent, HttpEventType} from '@angular/common/http';
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from 'rxjs';
import {Group} from 'src/app/shared/models/Group';
import {environment} from 'src/environments/environment';
import {GroupsApiService} from '../../shared/api';
import {UpdateGroupPayload} from '../../shared/models';
import {AuthUserService, ErrorService} from '../../shared/services';
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
    selector: 'app-group-settings-dialog',
    templateUrl: './group-settings-dialog.component.html',
    styleUrls: ['./group-settings-dialog.component.scss']
})
export class GroupSettingsDialogComponent implements OnInit, OnDestroy {

    form!: FormGroup;

    uploadProgress: number = 0;

    mediaUrl = environment.media_url;
    group: Group;
    confirmDelete: boolean = false;

    private subscriptions = new Subscription();

    constructor(
        private dialogref: MatDialogRef<GroupSettingsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { group: Group },
        private authUser: AuthUserService,
        private groupsApi: GroupsApiService,
        private formBuilder: FormBuilder,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.group = this.data.group;
        this.form = this.formBuilder.group({
            name: ['', [Validators.required]],
            avatar: [null],
            cover: [null]
        });
        this.initForm();
    }

    ngOnDestroy(): void {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    initForm(): void {
        this.form.get('name').patchValue(this.group.name);
    }

    onSubmit() {
        const payload: UpdateGroupPayload = {
            name: this.form.get('name').value
        }

        if (this.form.get('avatar').value) {
            payload.avatar = this.form.get('avatar').value;
            this.resetUpload();
        }

        if (this.form.get('cover').value) {
            payload.cover = this.form.get('cover').value;
            this.resetUpload();
        }

        this.subscriptions.add(
            this.groupsApi.updateSettings(this.group.id, payload).subscribe(
                (event: HttpEvent<any>) => {
                    if (event.type === HttpEventType.UploadProgress) {
                        this.uploadProgress = Math.round(100 * (event.loaded / event.total));
                    }

                    if (event.type === HttpEventType.Response) {
                        this.group = event.body.data;
                        this.snackBar.open('Successfully updated group settings.', 'X');
                    }
                },
                (error) => {
                    this.snackBar.open(this.errorService.handleErrors(error, this.form), 'X');
                }
            )
        );
    }

    private resetUpload() {
        this.uploadProgress = null;
    }


    onDelete() {
        this.confirmDelete = true;
    }

    onDeleteConfirm() {
        this.subscriptions.add(
            this.groupsApi.delete(this.group.id).subscribe(
                (result) => {
                    this.snackBar.open('Successfully deleted group.', 'X');
                    this.dialogref.close();
                    this.router.navigate(['groups']);
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error, this.form), 'X');
                }
            )
        )
    }

    hasFiles() {
        return this.form.get('avatar').value || this.form.get('cover').value;
    }

    clearCover() {
        this.form.get('cover').setValue(null);
    }

    clearAvatar() {
        this.form.get('avatar').setValue(null);
    }
}
