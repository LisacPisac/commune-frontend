import {NgModule} from '@angular/core';

import {GroupsRoutingModule} from './groups-routing.module';
import {GroupListComponent} from './group-list/group-list.component';
import {SharedModule} from '../shared/shared.module';
import {GroupPageComponent} from './group-page/group-page.component';
import {GroupSettingsDialogComponent} from './group-settings-dialog/group-settings-dialog.component';
import {CreateGroupComponent} from './create-group/create-group.component';


@NgModule({
    declarations: [
        GroupListComponent,
        GroupPageComponent,
        GroupSettingsDialogComponent,
        CreateGroupComponent
    ],
    imports: [
        SharedModule,
        GroupsRoutingModule
    ],
})
export class GroupsModule {
}
