import {HttpErrorResponse} from '@angular/common/http';
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {GroupsApiService, MembersApiService, PostsApiService} from 'src/app/shared/api';
import {Group, JoinGroupPayload, Member, Post} from 'src/app/shared/models';
import {AuthUserService} from 'src/app/shared/services/auth-user.service';
import {GlobalLoadingService} from 'src/app/shared/services/global-loading.service';
import {environment} from 'src/environments/environment';
import {GroupSettingsDialogComponent} from '../group-settings-dialog/group-settings-dialog.component';
import {
    PendingRequestsDialogComponent
} from "../../shared/components/dialogs/pending-requests-dialog/pending-requests-dialog.component";
import {debounceTime} from "rxjs/operators";
import {ErrorService} from "../../shared/services";

enum MembershipStatus {
    NONE,
    PENDING,
    MEMBER,
    ADMIN,
    OWNER
}

@Component({
    selector: 'app-group-page',
    templateUrl: './group-page.component.html',
    styleUrls: ['./group-page.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupPageComponent implements OnInit {

    readonly enum = MembershipStatus

    group: Group;
    posts: Post[] = [];
    members: Member[] = [];

    status: MembershipStatus = MembershipStatus.NONE;

    pendingRequests: Member[] = [];

    mediaUrl = environment.media_url;

    private postsPage = 1;
    private pageSize = 10;

    private postFilters = {
        page: this.postsPage,
        page_size: this.pageSize
    };

    private subscriptions: Subscription = new Subscription();

    get canPost() {
        return this.status === MembershipStatus.MEMBER || this.status === MembershipStatus.ADMIN || this.status === MembershipStatus.OWNER;
    }

    constructor(
        public authUser: AuthUserService,
        private postsApi: PostsApiService,
        private groupsApi: GroupsApiService,
        private membersApi: MembersApiService,
        private loadingService: GlobalLoadingService,
        private route: ActivatedRoute,
        private router: Router,
        private dialog: MatDialog,
        private snackBar: MatSnackBar,
        private changeDetectorRef: ChangeDetectorRef,
        private errorService: ErrorService
    ) {
    }

    ngOnInit(): void {
        this.group = this.route.snapshot.data.group;
        this.loadPosts();
        this.loadMembers();

        if (!this.isOwner()) {
            this.getMemberStatus();
        } else {
            this.status = MembershipStatus.OWNER;
            this.getPendingRequests();
        }
    }

    loadPosts() {
        this.loadingService.start();
        this.subscriptions.add(
            this.postsApi.getForGroup(this.group.id, this.postFilters).subscribe(
                (posts: Post[]) => {
                    if (posts.length > 0) {
                        this.posts = [...this.posts, ...posts];
                    } else {
                        this.postsPage--
                    }
                    this.loadingService.finish();
                    this.changeDetectorRef.markForCheck();
                },
                (error: HttpErrorResponse) => {
                    this.loadingService.finish();
                    this.changeDetectorRef.markForCheck();
                }
            )
        );
    }

    onPostCreated(post: Post) {
        if (post) {
            this.posts.unshift(post);
            this.changeDetectorRef.markForCheck();
        }
    }

    isOwner() {
        return this.authUser.userId === this.group.owner.id;
    }

    getMemberStatus() {
        this.loadingService.start();
        this.subscriptions.add(
            this.membersApi.status(this.group.id).subscribe(
                (member: Member) => {
                    this.status = MembershipStatus.MEMBER;

                    if (member.admin) {
                        this.status = MembershipStatus.ADMIN;
                        this.getPendingRequests();
                    }

                    if (member.pending) {
                        this.status = MembershipStatus.PENDING;
                    }

                    this.changeDetectorRef.markForCheck();
                    this.loadingService.finish();
                    console.debug(this.status);
                },
                (error: HttpErrorResponse) => {
                    if (error.status === 404) {
                        this.status = MembershipStatus.NONE;
                        this.changeDetectorRef.markForCheck();
                    }
                    this.loadingService.finish();
                    console.debug(this.status);
                }
            )
        )
    }

    openSettings() {
        if (this.status === MembershipStatus.ADMIN || this.status === MembershipStatus.OWNER) {
            const dialogRef = this.dialog.open(GroupSettingsDialogComponent, {data: {group: this.group}});

            this.subscriptions.add(
                dialogRef.afterClosed().subscribe(
                    (group: Group) => {
                        if (group) {
                            this.group = group;
                            this.changeDetectorRef.markForCheck();
                        }
                    }
                )
            );
        }
    }

    sendJoinRequest() {
        const payload: JoinGroupPayload = {
            user_id: this.authUser.userId
        }

        this.loadingService.start();
        this.subscriptions.add(
            this.groupsApi.join(this.group.id, payload)
                .pipe(debounceTime(500))
                .subscribe(
                    (response: Member) => {
                        this.loadingService.finish();
                        this.getMemberStatus();
                        this.changeDetectorRef.markForCheck();
                        this.snackBar.open('A request to join the group has been sent. Please wait until it is approved.', 'X')
                    },
                    (error: HttpErrorResponse) => {
                        this.loadingService.finish();
                        this.snackBar.open(this.errorService.handleErrors(error), 'X');
                        this.changeDetectorRef.markForCheck();
                    }
                )
        );
    }

    leave() {
        this.subscriptions.add(
            this.membersApi.delete(this.authUser.userId, this.group.id)
                .pipe(debounceTime(500))
                .subscribe(
                    (result) => {
                        this.status = MembershipStatus.NONE;
                        this.changeDetectorRef.markForCheck();
                        this.snackBar.open('You have left the group.', 'X', {duration: 4000})
                        return this.router.navigate(['groups']);
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    }
                )
        )
    }

    openRequests() {
        const dialogData = {
            requests: this.pendingRequests
        };
        const dialogRef = this.dialog.open(PendingRequestsDialogComponent, {data: dialogData});

        this.subscriptions.add(
            dialogRef.afterClosed().subscribe(
                (result) => {

                }
            )
        )
    }

    cancelRequest() {
        this.subscriptions.add(
            this.membersApi.delete(this.authUser.userId, this.group.id)
                .pipe(debounceTime(500))
                .subscribe(
                    (result) => {
                        this.status = MembershipStatus.NONE;
                        this.changeDetectorRef.markForCheck();
                        this.snackBar.open('The request to join the group has been cancelled.', 'X')
                    },
                    (error: HttpErrorResponse) => {

                    }
                )
        )
    }

    getPendingRequests() {
        this.subscriptions.add(
            this.groupsApi.getPendingRequests(this.group.id).subscribe(
                (members) => {
                    this.pendingRequests = members;
                }
            )
        );
    }

    loadPage() {
        this.postsPage++;
        this.setFilters();
        this.loadPosts()
    }

    loadMembers() {
        this.membersApi.getForGroup(this.group.id).subscribe(
            (members: Member[]) => {
                this.members = [...this.members, ...members];
                this.changeDetectorRef.markForCheck();
            }
        )
    }

    private setFilters() {
        this.postFilters = {
            page: this.postsPage,
            page_size: this.pageSize
        };
    }

    kickMember(memberToDelete: Member) {
        this.subscriptions.add(
            this.membersApi.delete(memberToDelete.user.id, this.group.id)
                .pipe(debounceTime(500))
                .subscribe(
                    (result) => {
                        this.members = this.members.filter((member) => member.user.id !== memberToDelete.user.id);
                        this.changeDetectorRef.markForCheck();
                        this.snackBar.open('The member has been kicked.', 'X')
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    }
                )
        )
    }
}
