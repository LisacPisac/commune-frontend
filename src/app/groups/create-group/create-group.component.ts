import {Component, EventEmitter, Output} from '@angular/core';
import {Group} from "../../shared/models";
import {Subscription} from "rxjs";
import {AuthUserService} from "../../shared/services/auth-user.service";
import {MatDialog} from "@angular/material/dialog";
import {CreateGroupDialogComponent} from "../../shared/components/dialogs/create-group-dialog/create-group-dialog.component";

@Component({
    selector: 'app-create-group',
    templateUrl: './create-group.component.html',
    styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent {

    @Output() group = new EventEmitter<Group>();

    private subscription: Subscription;

    constructor(
        private authService: AuthUserService,
        private dialog: MatDialog,
    ) {
    }

    openGroupDialog() {
        const dialogRef = this.dialog.open(CreateGroupDialogComponent, {maxHeight: 800});

        this.subscription = dialogRef.afterClosed().subscribe(
            (result: Group) => {
                this.group.emit(result);
            },
            (error) => {

            }
        );
    }
}
