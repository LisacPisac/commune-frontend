import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GroupResolver} from '../shared/resolvers/group.resolver';
import {GroupListComponent} from './group-list/group-list.component';
import {GroupPageComponent} from './group-page/group-page.component';

const routes: Routes = [
    {
        path: '',
        component: GroupListComponent
    },
    {
        path: ':groupId',
        component: GroupPageComponent,
        runGuardsAndResolvers: 'always',
        resolve: {group: GroupResolver}
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GroupsRoutingModule { }
