import {NgModule} from '@angular/core';
import {GlobalSearchComponent} from './global-search/global-search.component';
import {SearchRoutingModule} from "./search-routing.module";
import {SharedModule} from "../shared/shared.module";


@NgModule({
    declarations: [
        GlobalSearchComponent
    ],
    imports: [
        SharedModule,
        SearchRoutingModule,
    ]
})
export class SearchModule {
}
