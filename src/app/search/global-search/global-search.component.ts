import {AuthUserService, GlobalLoadingService} from '../../shared/services';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FormControl} from "@angular/forms";
import {Subscription} from "rxjs";
import {SearchResult} from "../../shared/models";
import {SearchApiService} from "../../shared/api/search-api.service";
import {debounceTime} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";
import {MatTabGroup} from "@angular/material/tabs";

@Component({
    selector: 'app-global-search',
    templateUrl: './global-search.component.html',
    styleUrls: ['./global-search.component.scss']
})
export class GlobalSearchComponent implements OnInit, OnDestroy {

    @ViewChild('matTabGroup') matTabGroup: MatTabGroup;

    searchFc = new FormControl();

    results: SearchResult;

    private subscriptions = new Subscription();

    constructor(
        private authUser: AuthUserService,
        private searchApi: SearchApiService,
        private globalLoadingService: GlobalLoadingService
    ) {
    }

    ngOnInit(): void {
        this.subscriptions.add(
            this.searchFc.valueChanges
                .pipe(debounceTime(500))
                .subscribe(
                    ((value: string) => {
                        if (value.trim()) {
                            if (value.startsWith('@')) {
                                this.searchByUser();
                            } else if (value.startsWith('#')) {
                                this.searchByTag();
                            } else {
                                this.loadSearch();
                            }
                        } else {
                            this.results = null;
                        }
                    })
                )
        )
    }

    ngOnDestroy(): void {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    loadSearch() {
        this.globalLoadingService.start();
        this.subscriptions.add(
            this.searchApi.globalSearch(this.searchFc.value)
                .subscribe(
                    (result) => {
                        this.globalLoadingService.finish();
                        this.results = result;
                    },
                    (error: HttpErrorResponse) => {
                        this.globalLoadingService.finish();
                    }
                )
        );
    }

    searchByUser() {
        const value: string = this.searchFc.value.replace('@', '');
        this.globalLoadingService.start();
        this.subscriptions.add(
            this.searchApi.searchByUser(value)
                .subscribe(
                    (result) => {
                        this.globalLoadingService.finish();
                        this.matTabGroup.selectedIndex = 1;
                        this.results = result;
                    },
                    (error: HttpErrorResponse) => {
                        this.globalLoadingService.finish();
                    }
                )
        );
    }

    searchByTag() {
        const value: string = this.searchFc.value.replace('#', '');
        this.globalLoadingService.start();
        this.subscriptions.add(
            this.searchApi.searchByTag(value)
                .subscribe(
                    (result) => {
                        this.globalLoadingService.finish();
                        this.matTabGroup.selectedIndex = 1;
                        this.results = result;
                    },
                    (error: HttpErrorResponse) => {
                        this.globalLoadingService.finish();
                    }
                )
        );
    }
}
