import {Component, OnInit} from '@angular/core';
import {AuthUserService} from "../../shared/services";
import {BillingApiService} from "../../shared/api";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-billing-cancel',
    templateUrl: './billing-cancel.component.html',
    styleUrls: ['./billing-cancel.component.scss']
})
export class BillingCancelComponent implements OnInit {

    private subscription: Subscription;

    constructor(
        private authUser: AuthUserService,
        private billingApi: BillingApiService,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.subscription = this.billingApi.cancelOrder().subscribe(
            (result) => {
                return this.router.navigate(['profile']);
            },
            (error: HttpErrorResponse) => {

            }
        )
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

}
