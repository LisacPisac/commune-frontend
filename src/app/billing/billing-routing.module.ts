import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BillingSuccessComponent} from "./billing-success/billing-success.component";
import {BillingCancelComponent} from "./billing-cancel/billing-cancel.component";

const routes: Routes = [
    {
        path: 'success',
        component: BillingSuccessComponent
    },
    {
        path: 'cancel',
        component: BillingCancelComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BillingRoutingModule {
}
