import {NgModule} from '@angular/core';

import {BillingRoutingModule} from './billing-routing.module';
import {BillingSuccessComponent} from './billing-success/billing-success.component';
import {BillingCancelComponent} from './billing-cancel/billing-cancel.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
    declarations: [
        BillingSuccessComponent,
        BillingCancelComponent
    ],
    imports: [
        SharedModule,
        BillingRoutingModule
    ]
})
export class BillingModule {
}
