import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthUserService, ErrorService} from "../../shared/services";
import {BillingApiService} from "../../shared/api";
import {Subscription} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {Router} from "@angular/router";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-billing-success',
    templateUrl: './billing-success.component.html',
    styleUrls: ['./billing-success.component.scss']
})
export class BillingSuccessComponent implements OnInit, OnDestroy {

    private subscription: Subscription;

    constructor(
        private authUser: AuthUserService,
        private billingApi: BillingApiService,
        private router: Router,
        private snackBar: MatSnackBar,
        private errorService: ErrorService
    ) {
    }

    ngOnInit(): void {
        this.subscription = this.billingApi.captureOrder().subscribe(
            (result) => {
                setTimeout(
                    () => this.router.navigate(['profile']),
                    10000
                );
            },
            (error: HttpErrorResponse) => {
                this.snackBar.open(this.errorService.handleErrors(error), 'X');
            }
        )
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
