import {NgModule} from '@angular/core';

import {ChatRoutingModule} from './chat-routing.module';
import {MainChatComponent} from './main-chat/main-chat.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
    declarations: [
        MainChatComponent,
    ],
    imports: [
        SharedModule,
        ChatRoutingModule,
    ]
})
export class ChatModule {
}
