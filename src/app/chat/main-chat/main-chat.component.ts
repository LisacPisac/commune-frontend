import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from "rxjs";
import {FormControl} from "@angular/forms";
import {Chat, SendMessagePayload} from "../../shared/models/Chat";
import {LoadMessagesRequest, Message} from "../../shared/models/Message";
import {AuthUserService, GlobalLoadingService} from "../../shared/services";
import {ChatApiService, ContactsApiService} from "../../shared/api";
import {ContactStatusEnum, User} from "../../shared/models";
import {environment} from "../../../environments/environment";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-main-chat',
    templateUrl: './main-chat.component.html',
    styleUrls: ['./main-chat.component.scss']
})
export class MainChatComponent implements OnInit, OnDestroy {

    messageFc = new FormControl('');
    chats: Chat[] = [];
    activeChat: Chat;
    messages: Message[] = [];

    msgPage = 1;

    mediaUrl = environment.media_url;

    sendingMessage = false;
    isContact = false;
    isDeleted = false;
    canPoll = false;

    private subscriptions = new Subscription();
    private interval: any;

    constructor(
        private authUser: AuthUserService,
        private contactsApi: ContactsApiService,
        private chatApi: ChatApiService,
        private loadingService: GlobalLoadingService,
        private snackBar: MatSnackBar
    ) {
    }

    ngOnInit(): void {
        this.loadChats();

        this.interval = setInterval(
            () => {
                if (this.activeChat && this.canPoll) {
                    this.pollMessages();
                }
            },
            1000
        );
    }

    ngOnDestroy() {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }

        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    loadChats() {
        this.loadingService.start();
        this.subscriptions.add(
            this.chatApi.indexChats().subscribe(
                (result) => {
                    this.chats = result;
                    this.loadingService.finish();
                },
                (error: HttpErrorResponse) => {
                    this.loadingService.finish();
                }
            )
        )
    }

    loadMessages() {
        const loadMessagesRequest: LoadMessagesRequest = {
            page: this.msgPage,
            chat_id: this.activeChat.id
        }

        this.loadingService.start();

        this.subscriptions.add(
            this.chatApi.indexMessages(loadMessagesRequest).subscribe(
                (result) => {
                    if (result.length > 0) {
                        this.messages = [...new Set([...this.messages, ...result])].reverse();
                    } else {
                        this.msgPage--;
                    }
                    this.loadingService.finish();
                    this.canPoll = true;
                }
            )
        )
    }

    pollMessages() {
        const loadMessagesRequest: LoadMessagesRequest = {
            page: 1,
            chat_id: this.activeChat.id
        }

        this.subscriptions.add(
            this.chatApi.indexMessages(loadMessagesRequest).subscribe(
                (result) => {
                    // this.messages = [...new Set([...this.messages, ...result.reverse()])];
                    result.reverse().forEach(
                        (message) => {
                            if (!this.messages.find((msg) => message.id === msg.id)) {
                                this.messages.push(message);
                            }
                        }
                    )
                    this.loadingService.finish();
                }
            )
        )
    }

    sendMsg(event?: KeyboardEvent) {
        if (this.messageFc.value !== '' && (event === undefined || event.key === 'Enter')) {
            this.sendingMessage = true;

            const payload: SendMessagePayload = {
                chat_id: this.activeChat.id,
                text: this.messageFc.value
            }

            this.messageFc.patchValue('');
            this.subscriptions.add(
                this.chatApi.sendMessage(payload).subscribe(
                    (result) => {
                        this.messages.push(result);
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open("Unfortunately, your message could not be delivered.", "Dismiss", {duration: 5000});
                    }
                )
            )
        }
    }

    getChatAvatar(chat: Chat) {
        let avatar: string = 'assets/resources/img/default_avatar_48dp.svg';
        let user: User;

        user = chat.users.find(
            (user: User) => user.id !== this.authUser.userId
        );

        if (!user.is_deleted) {
            if (user.avatar) {
                avatar = this.mediaUrl + user.avatar;
            }
        }

        return avatar;
    }

    getChatName(chat: Chat) {
        if (chat.name) {
            return chat.name;
        } else {
            let name: string = 'Deleted user';
            let user: User;

            user = chat.users.find(
                (user: User) => user.id !== this.authUser.userId
            );

            if (!user.is_deleted) {
                name = user.username;
            }

            return name;
        }
    }

    onOpenChat(chat: Chat) {
        if (!this.activeChat || chat.id !== this.activeChat?.id) {
            this.canPoll = false;
            this.activeChat = chat;
            this.msgPage = 1;
            this.messages = [];

            this.getIsDeleted();
            this.getContactStatus();
            this.loadMessages();
        }
    }

    isUsersMessage(message: Message) {
        return message.user.id === this.authUser.userId;
    }

    getContactStatus() {
        this.activeChat.users.forEach(
            (user: User) => {
                if (user.id !== this.authUser.userId) {
                    this.subscriptions.add(
                        this.contactsApi.status(user.id, this.authUser.userId).subscribe(
                            (response) => {
                                this.isContact = response.status === ContactStatusEnum.CONTACTS;
                            }
                        )
                    );
                }
            }
        );
    }

    getIsDeleted() {
        this.isDeleted = false;

        this.activeChat.users.forEach(
            (user: User) => {
                if (user.id !== this.authUser.userId) {
                    if (user.is_deleted) {
                        this.isDeleted = true;
                    }
                }
            }
        );
    }

    onLoad() {
        this.canPoll = false;
        this.msgPage++;
        this.loadMessages()
    }
}
