import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {RegistrationRequestPayload} from "../../shared/models";
import {UsersApiService} from "../../shared/api";
import {Subscription} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {ErrorService} from "../../shared/services";

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

    form!: FormGroup;

    success: boolean = false;

    private subscription!: Subscription;

    constructor(
        private formBuilder: FormBuilder,
        private usersApi: UsersApiService,
        private errorService: ErrorService
    ) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required]],
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
            password2: ['', [Validators.required]]
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onSubmit() {
        const payload: RegistrationRequestPayload = {
            email: this.form.get('email')?.value,
            username: this.form.get('username')?.value,
            password: this.form.get('password')?.value
        }

        this.subscription = this.usersApi.register(payload).subscribe(
            (response) => {
                this.success = true;
            },
            (error: HttpErrorResponse) => {
                this.errorService.handleErrors(error, this.form);
            }
        );
    }

}
