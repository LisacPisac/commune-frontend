import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {LogoutComponent} from "./logout/logout.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {VerifyComponent} from "./verify/verify.component";

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'login'
    },
    {
        path: 'login',
        component: LoginComponent
    },
    {
        path: 'pw-reset',
        component: ResetPasswordComponent
    },
    {
        path: 'password-reset/:token',
        component: ResetPasswordComponent
    },
    {
        path: 'verify/:token',
        component: VerifyComponent
    },
    {
        path: 'register',
        component: RegisterComponent
    },
    {
        path: 'logout',
        component: LogoutComponent
    },
    // {
    //     path: 'about',
    //     component: AboutComponent,
    // }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule {
}
