import {NgModule} from '@angular/core';

import {PublicRoutingModule} from './public-routing.module';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {SharedModule} from "../shared/shared.module";
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {LogoutComponent} from './logout/logout.component';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {VerifyComponent} from './verify/verify.component';
import {AboutComponent} from './about/about.component';


@NgModule({
    declarations: [
        LoginComponent,
        RegisterComponent,
        PageNotFoundComponent,
        LogoutComponent,
        ResetPasswordComponent,
        VerifyComponent,
        AboutComponent
    ],
    imports: [
        SharedModule,
        PublicRoutingModule,
    ]
})
export class PublicModule {
}
