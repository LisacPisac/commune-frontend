import {Component, OnInit} from '@angular/core';
import {AuthApiService} from "../../shared/api";
import {ErrorService} from "../../shared/services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-verify',
    templateUrl: './verify.component.html',
    styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
    success: boolean = false;
    error: boolean = false;
    token: string;

    private subscription: Subscription;

    constructor(
        private authApi: AuthApiService,
        private errorService: ErrorService,
        private snackBar: MatSnackBar,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit(): void {
        this.token = this.activatedRoute.snapshot.paramMap.get('token');

        if (this.token) {
            this.subscription = this.authApi.verify(this.token).subscribe(
                (result) => {
                    this.success = result;
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    this.error = true;
                }
            );
        } else {
            this.error = true;
        }
    }
}
