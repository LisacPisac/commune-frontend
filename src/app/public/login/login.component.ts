import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AuthApiService, UsersApiService} from '../../shared/api';
import {LoginRequestPayload, User} from '../../shared/models';
import {AuthUserService, ErrorService} from '../../shared/services';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

    form!: FormGroup;
    tfaEnabled: boolean = false;

    private subscription: Subscription;

    constructor(
        private loginApi: AuthApiService,
        private userApi: UsersApiService,
        private formBuilder: FormBuilder,
        private authUser: AuthUserService,
        private router: Router,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
            tfa_code: [''],
        });

        this.subscription = this.userApi.getProfile().subscribe(
            (profile) => {
                this.authUser.user = profile;
                this.router.navigate(['/feed']);
            }
        );
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onSubmit() {
        this.form.markAllAsTouched();
        const payload: LoginRequestPayload = {
            email: this.form.get('email')?.value,
            password: this.form.get('password')?.value,
        };

        this.subscription = this.loginApi.login(payload).subscribe(
            (response: User) => {
                if (!response.tfa_enabled) {
                    this.authUser.user = response;
                    this.router.navigate(['/feed']);
                } else {
                    this.tfaEnabled = true;
                }
            },
            (error: HttpErrorResponse) => {
                if (error.status === 404) {
                    this.form.setErrors({not_found: 'No existing user.'});
                } else {
                    this.errorService.handleErrors(error, this.form);
                }
            },
        );
    }

    onSubmitTfa() {
        const payload: LoginRequestPayload = {
            email: this.form.get('email')?.value,
            password: this.form.get('password')?.value,
            tfa_code: this.form.get('tfa_code')?.value,
        };

        this.subscription = this.loginApi.login(payload).subscribe(
            (response: User) => {
                this.authUser.user = response;
                this.router.navigate(['/feed']);
            },
            (error: HttpErrorResponse) => {
                this.errorService.handleErrors(error, this.form);
            },
        );
    }
}
