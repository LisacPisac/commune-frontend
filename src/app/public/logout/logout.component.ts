import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthApiService} from "../../shared/api/auth-api.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-logout',
    templateUrl: './logout.component.html',
    styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit, OnDestroy {

    private subscription: Subscription;

    constructor(
        private authApi: AuthApiService,
        private router: Router,
    ) {
    }

    ngOnInit(): void {
        this.subscription = this.authApi.logout().subscribe(
            (value) => this.router.navigate(['/'])
        );
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
