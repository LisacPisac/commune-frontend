import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthApiService} from "../../shared/api";
import {Subscription} from "rxjs";
import {ActivatedRoute, Router} from "@angular/router";
import {passwordResetValidator} from "../../shared/validators/passwordResetValidator";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
})
export class ResetPasswordComponent implements OnInit, OnDestroy {

    form: FormGroup;
    token: string = null;

    sent: boolean = false;

    private subscription: Subscription;

    constructor(
        private authApi: AuthApiService,
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router
    ) {
    }

    ngOnInit(): void {
        this.token = this.route.snapshot.paramMap.get('token');
        if (this.token) {
            this.form = this.formBuilder.group({
                password: ['', [Validators.required]],
                password_repeat: ['', [Validators.required]],
            }, {validators: passwordResetValidator});
        } else {
            this.form = this.formBuilder.group({
                email: ['', [Validators.required, Validators.email]]
            });
        }
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onSubmit() {
        if (this.form.valid) {
            if (this.token) {
                this.subscription = this.authApi.resetPassword(this.token, this.form.get('password').value).subscribe(
                    (user) => {
                        this.router.navigate(['/login']);
                    },
                    (error: HttpErrorResponse) => {
                        if (error.status === 404) {
                            this.form.setErrors({token: 'Token is invalid or has expired.'});
                        }
                    }
                )
            } else {
                this.subscription = this.authApi.initiatePasswordReset(this.form.get('email').value).subscribe(
                    (result) => {
                        this.sent = true;
                    }
                );
            }
        }
    }
}
