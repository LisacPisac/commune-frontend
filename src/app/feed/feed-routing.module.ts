import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainFeedComponent} from "./main-feed/main-feed.component";

const routes: Routes = [
    {
        path: '',
        component: MainFeedComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FeedRoutingModule {
}
