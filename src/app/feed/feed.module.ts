import {NgModule} from '@angular/core';

import {FeedRoutingModule} from './feed-routing.module';
import {MainFeedComponent} from './main-feed/main-feed.component';
import {SharedModule} from "../shared/shared.module";


@NgModule({
    declarations: [
        MainFeedComponent
    ],
    imports: [
        SharedModule,
        FeedRoutingModule
    ]
})
export class FeedModule {
}
