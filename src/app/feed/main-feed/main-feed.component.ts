import {HttpErrorResponse} from '@angular/common/http';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostsApiService} from "../../shared/api";
import {IndexPostsPayload, Post} from "../../shared/models";
import {AuthUserService, ErrorService, GlobalLoadingService} from "../../shared/services";
import {Subscription} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-main-feed',
    templateUrl: './main-feed.component.html',
    styleUrls: ['./main-feed.component.scss']
})
export class MainFeedComponent implements OnInit, OnDestroy {

    feedItems: Post[] = [];

    private page = 1;
    private pageSize = 10;

    private filters: IndexPostsPayload;

    private subscription = new Subscription();

    constructor(
        private authUser: AuthUserService,
        private postApi: PostsApiService,
        private loadingService: GlobalLoadingService,
        private errorService: ErrorService,
        private snackBar: MatSnackBar
    ) {
    }

    ngOnInit(): void {
        this.setFilters();
        this.loadPosts();
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    loadPosts() {
        this.loadingService.start();
        this.subscription.add(
            this.postApi.index(this.filters)
                .subscribe(
                    (result) => {
                        if (result.length > 0) {
                            this.feedItems = [...this.feedItems, ...result];
                        } else {
                            this.page--;
                        }
                        this.loadingService.finish();
                    },
                    (error: HttpErrorResponse) => {
                        this.loadingService.finish();
                        this.snackBar.open(this.errorService.handleErrors(error), 'X');
                    }
                )
        );
    }

    loadPage() {
        this.page++;
        this.setFilters();
        this.loadPosts()
    }

    private setFilters() {
        this.filters = {
            page: this.page,
            page_size: this.pageSize
        };
    }

    onPostCreated(post: Post) {
        if (post) {
            this.feedItems.unshift(post);
        }
    }
}
