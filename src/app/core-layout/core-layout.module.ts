import {NgModule, Optional, SkipSelf} from '@angular/core';

import {CoreLayoutRoutingModule} from './core-layout-routing.module';
import {NavbarComponent} from "./navbar/navbar.component";
import {CoreLayoutComponent} from "./core-layout.component";
import {SharedModule} from "../shared/shared.module";
import {NavigationMenuService} from "./services/navigation-menu.service";


@NgModule({
    declarations: [
        NavbarComponent,
        CoreLayoutComponent,
    ],
    imports: [
        SharedModule,
        CoreLayoutRoutingModule,
    ],
    providers: [
        NavigationMenuService
    ]
})
export class CoreLayoutModule {
    constructor(@Optional() @SkipSelf() parentModule: CoreLayoutModule) {
        if (parentModule) {
            throw new Error('CoreLayoutModule is already loaded by AppModule.');
        }
    }
}
