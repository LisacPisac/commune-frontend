import {Injectable} from '@angular/core';

export type MenuItem = {
    label: string;
    target: string;
    icon: string;
}

@Injectable({
    providedIn: 'root'
})
export class NavigationMenuService {

    constructor() {
    }

    generateMenu(): MenuItem[] {
        return [
            {
                label: 'Home',
                target: '/feed',
                icon: 'view_headline'
            },
            {
                label: 'Groups',
                target: '/groups',
                icon: 'groups'
            },
            {
                label: 'Chat',
                target: '/chat',
                icon: 'chat'
            },
            {
                label: 'Search',
                target: '/search',
                icon: 'search'
            },
            // {
            //     label: 'About',
            //     target: '/about',
            //     icon: 'info'
            // },
            {
                label: 'Logout',
                target: '/logout',
                icon: 'logout'
            },
        ];
    }
}
