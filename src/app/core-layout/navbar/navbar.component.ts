import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {Component, OnInit} from '@angular/core';
import {NavigationEnd, NavigationStart, Router} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {AuthUserService, GlobalLoadingService} from "../../shared/services";
import {MenuItem, NavigationMenuService} from "../services/navigation-menu.service";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    providers: [GlobalLoadingService]
})
export class NavbarComponent implements OnInit {

    menuItems: MenuItem[] = [];

    isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches),
            shareReplay()
        );

    subscription: Subscription;
    mediaUrl = environment.media_url;

    constructor(
        private breakpointObserver: BreakpointObserver,
        private navigationMenuService: NavigationMenuService,
        private router: Router,
        public authUser: AuthUserService,
        public loadingService: GlobalLoadingService
    ) {
    }

    ngOnInit() {
        this.menuItems = this.navigationMenuService.generateMenu();

        this.subscription = this.router.events.subscribe(
            (event) => {
                if (event instanceof NavigationStart) {
                    this.loadingService.start();
                }

                if (event instanceof NavigationEnd) {
                    this.loadingService.finish();
                }
            }
        )
    }
}
