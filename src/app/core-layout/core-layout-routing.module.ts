import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ProfileResolver} from "../shared/resolvers/profile-resolver.service";
import {CoreLayoutComponent} from "./core-layout.component";

const routes: Routes = [
    {
        path: '',
        component: CoreLayoutComponent,
        runGuardsAndResolvers: "always",
        resolve: {user: ProfileResolver},
        children: [
            {
                path: '',
                loadChildren: () => import('../public/public.module').then(m => m.PublicModule),
            },
            {
                path: 'feed',
                loadChildren: () => import('../feed/feed.module').then(m => m.FeedModule),
            },
            {
                path: 'groups',
                loadChildren: () => import('../groups/groups.module').then(m => m.GroupsModule),
            },
            {
                path: 'profile',
                loadChildren: () => import('../profile/profile.module').then(m => m.ProfileModule),
            },
            {
                path: 'search',
                loadChildren: () => import('../search/search.module').then(m => m.SearchModule),
            },
            {
                path: 'chat',
                loadChildren: () => import('../chat/chat.module').then(m => m.ChatModule),
            },
            {
                path: 'billing',
                loadChildren: () => import('../billing/billing.module').then(m => m.BillingModule)
            }
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CoreLayoutRoutingModule {
}
