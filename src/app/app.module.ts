import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SharedModule} from "./shared/shared.module";
import {AuthUserService} from "./shared/services";
import {PublicModule} from "./public/public.module";
import {AuthInterceptor} from "./shared/interceptors/auth.interceptor";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        SharedModule.forRoot(),
        BrowserAnimationsModule,
        PublicModule,
        AppRoutingModule,
    ],
    exports: [SharedModule],
    providers: [AuthUserService, {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true}],
    bootstrap: [AppComponent],
})
export class AppModule {
}
