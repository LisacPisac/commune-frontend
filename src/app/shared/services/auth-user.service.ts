import {Injectable, Optional, SkipSelf} from '@angular/core';
import {User} from "../models/User";

@Injectable({
    providedIn: 'root'
})
export class AuthUserService {

    private _user: User;

    constructor(@Optional() @SkipSelf() parent?: AuthUserService) {
        if (parent) {
            throw Error('User service is a singleton already provided by the root App module.');
        }
    }

    set user(value: User) {
        this._user = value;
    }

    get user(): User {
        return this._user;
    }

    get userId(): number {
        return this._user.id;
    }

    validateUser() {

    }
}
