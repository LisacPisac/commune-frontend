import {Injectable} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {Error} from "../models";
import {HttpErrorResponse} from "@angular/common/http";

@Injectable({
    providedIn: 'root'
})
export class ErrorService {

    constructor() {
    }

    handleErrors(errorResponse: HttpErrorResponse, formGroup ?: FormGroup): string {
        if (errorResponse.status === 500) {
            return 'An internal server error occurred';
        }

        if (errorResponse.status === 422) {
            let errors: Error[] = errorResponse?.error?.data?.errors;

            if (errors) {
                formGroup.setErrors({});

                let formErrors = {};
                errors.forEach(
                    (error: Error) => {
                        let errorObj: { [key: string]: any } = {};
                        errorObj[error.field] = error.message;
                        formErrors = Object.assign(formErrors, errorObj);

                        formGroup.get(error.field).setErrors(errorObj);
                    }
                );

                formGroup.setErrors(formErrors);
            }

            return 'The data you have supplied is incorrect';
        }

        return 'An internal server error occurred';
    }
}
