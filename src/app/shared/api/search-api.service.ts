import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {SearchResult} from "../models";
import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class SearchApiService {

    readonly basePath = environment.api_url + 'search/';

    constructor(
        private http: HttpClient
    ) {
    }

    globalSearch(searchTerm: string): Observable<SearchResult> {
        const path = this.basePath + 'global';

        return this.http.get(path, {params: {search: searchTerm.trim()}}).pipe(
            map((response: any) => response.data)
        );
    }

    searchByUser(username: string) {
        const path = this.basePath + 'user';

        return this.http.get(path, {params: {username: username.trim()}}).pipe(
            map((response: any) => response.data)
        );
    }

    searchByTag(tag: string) {
        const path = this.basePath + 'tag';

        return this.http.get(path, {params: {tag: tag.trim()}}).pipe(
            map((response: any) => response.data)
        );
    }
}
