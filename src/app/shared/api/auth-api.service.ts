import {HttpClient} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {ActivateTfaPayload, LoginRequestPayload, TfaSecret, User} from "../models";

@Injectable({
    providedIn: 'root'
})
export class AuthApiService {

    readonly basePath = `${environment.api_url}`;

    constructor(
        private http: HttpClient
    ) {
    }

    login(payload: LoginRequestPayload): Observable<User> {
        const path = this.basePath + 'login';

        return this.http.post<User>(path, payload).pipe(
            map(response => (response as any).data)
        );
    }

    logout(): Observable<any> {
        const path = this.basePath + 'logout';

        return this.http.post<any>(path, {});
    }

    initiatePasswordReset(email: string): Observable<any> {
        const path = this.basePath + 'init-password-reset';

        return this.http.post(path, {email: email}).pipe();
    }

    resetPassword(token: string, password: string): Observable<User> {
        const path = this.basePath + 'reset-password';

        const payload = {
            token: token,
            password: password
        }

        return this.http.post(path, payload).pipe(
            map((response: any) => response.data)
        );
    }

    generateTwoFactorSecret(): Observable<TfaSecret> {
        const path = this.basePath + 'generate-tfa-secret';

        return this.http.get(path).pipe(
            map((response: any) => response.data)
        )
    }

    activateTfa(payload: ActivateTfaPayload): Observable<User> {
        const path = this.basePath + 'activate-tfa';

        return this.http.post(path, payload).pipe(
            map((response: any) => response.data)
        );
    }

    deactivateTfa(): Observable<User> {
        const path = this.basePath + 'deactivate-tfa';

        return this.http.post(path, {}).pipe(
            map((response: any) => response.data)
        );
    }

    verify(token: string): Observable<boolean> {
        const path = this.basePath + 'verify';

        return this.http.post(path, {token: token}).pipe(
            map((response: any) => response.data)
        );
    }
}
