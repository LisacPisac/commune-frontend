import {HttpClient, HttpEvent, HttpParams} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {environment} from 'src/environments/environment';
import {Group, IndexGroupsPayload, JoinGroupPayload, Member, UpdateGroupPayload} from '../models';

@Injectable({
    providedIn: 'root'
})
export class GroupsApiService {

    private basePath = `${environment.api_url}groups/`;

    constructor(
        private http: HttpClient
    ) {
    }

    index(payload: IndexGroupsPayload): Observable<Group[]> {
        const path = this.basePath + 'index';

        const httpParams = new HttpParams().set('page', payload.page).set('limit', payload.limit);

        return this.http.get(path, {params: httpParams}).pipe(
            map(response => (<any>response).data)
        )
    }

    get(groupId: number): Observable<Group> {
        const path = this.basePath + 'get/' + groupId;

        return this.http.get(path).pipe(
            map(response => (<any>response).data)
        )
    }

    create(payload: any): Observable<Group> {
        const path = this.basePath + 'create';

        return this.http.post(path, payload).pipe(
            map(response => (<any>response).data)
        );
    }

    updateSettings(groupId: number, payload: UpdateGroupPayload): Observable<HttpEvent<any>> {
        const path = this.basePath + 'update/' + groupId;

        const formData = new FormData();
        formData.append('name', payload.name);

        if (payload.avatar) {
            formData.append('avatar', payload.avatar, payload.avatar.name)
        }

        if (payload.cover) {
            formData.append('cover', payload.cover, payload.cover.name)
        }

        return this.http.post<HttpEvent<any>>(path, formData, {reportProgress: true, observe: 'events'});
    }

    delete(groupId: number): Observable<any> {
        const path = this.basePath + 'delete/' + groupId;

        return this.http.delete(path).pipe(
            map((response: any) => response.data)
        );
    }

    join(groupId: number, payload: JoinGroupPayload): Observable<Member> {
        const path = this.basePath + 'join/' + groupId;

        return this.http.post(path, payload).pipe(
            map(response => (<any>response).data)
        )
    }

    getPendingRequests(groupId: number): Observable<Member[]> {
        const path = this.basePath + 'pending-requests/' + groupId;

        return this.http.get(path).pipe(
            map(response => (<any>response).data)
        )
    }
}
