import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {Observable} from "rxjs";
import {Message} from "../models/Message";
import {Chat, SendMessagePayload} from "../models/Chat";

@Injectable({
    providedIn: 'root'
})
export class ChatApiService {

    readonly basePath = environment.api_url + 'chats/';

    constructor(
        private http: HttpClient
    ) {
    }

    indexChats(): Observable<Chat[]> {
        const path = this.basePath + 'chats';

        return this.http.get(path).pipe(
            map((response: any) => response.data)
        );
    }

    indexMessages(payload: any): Observable<Message[]> {
        const path = this.basePath + 'messages';

        return this.http.get(path, {params: payload}).pipe(
            map((response: any) => response.data)
        );
    }

    createChat(contactUserId: number): Observable<Chat> {
        const path = this.basePath + 'chats';

        const payload = {
            contact_user_id: contactUserId
        }

        return this.http.post(path, payload).pipe(
            map((response: any) => response.data)
        );
    }

    sendMessage(payload: SendMessagePayload): Observable<Message> {
        const path = this.basePath + 'messages';

        return this.http.post(path, payload).pipe(
            map((response: any) => response.data)
        );
    }
}
