import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {
    Comment,
    CreateCommentRequestPayload,
    EditCommentRequestPayload,
    GetCommentRequestPayload,
    ReplyCommentRequestPayload
} from "../models";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class CommentsApiService {

    private basePath = `${environment.api_url}comments/`;

    constructor(
        private http: HttpClient
    ) {
    }

    get(payload: GetCommentRequestPayload): Observable<Comment[]> {
        const path = this.basePath + "get";

        const params = new HttpParams();
        params.set('page', payload.page);
        params.set('post_id', payload.post_id)
        params.set('comment_id', payload.comment_id)

        return this.http.get(path, {params: params}).pipe(
            map(response => (response as any).data)
        );
    }

    create(payload: CreateCommentRequestPayload): Observable<Comment> {
        const path = this.basePath + "create";
        return this.http.post(path, payload).pipe(
            map(response => (response as any).data)
        );
    }

    edit(commentId: number, payload: EditCommentRequestPayload): Observable<Comment> {
        const path = this.basePath + 'edit/' + commentId;

        return this.http.post(path, payload).pipe(
            map(response => (response as any).data)
        );
    }

    reply(payload: ReplyCommentRequestPayload): Observable<Comment> {
        const path = this.basePath + 'reply';

        return this.http.post(path, payload).pipe(
            map(response => (response as any).data)
        );
    }
}
