import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Member, UpdateMembershipPayload} from "../models";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class MembersApiService {

    private basePath = `${environment.api_url}members/`;

    constructor(private http: HttpClient) {
    }

    getForGroup(groupId: number): Observable<Member[]> {
        const path = this.basePath + 'group/' + groupId;

        return this.http.get(path).pipe(
            map(response => (<any>response).data)
        );
    }

    status(groupId: number): Observable<Member> {
        const path = this.basePath + 'status/' + groupId;

        return this.http.get<Member>(path).pipe(
            map(response => (<any>response).data)
        );
    }

    update(payload: UpdateMembershipPayload): Observable<Member> {
        const path = this.basePath + 'update';

        return this.http.post<Member>(path, payload).pipe(
            map(response => (<any>response).data)
        );
    }

    delete(userId: number, groupId: number): Observable<any> {
        const path = this.basePath + 'decline';
        const params = new HttpParams()
            .set('user_id', userId)
            .set('group_id', groupId);

        return this.http.delete<any>(path, {params: params}).pipe(
            map(response => (<any>response).data)
        );
    }
}
