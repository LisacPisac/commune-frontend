import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {Contact, ContactStatusEnum, User} from "../models";

@Injectable({
    providedIn: 'root'
})
export class ContactsApiService {

    readonly basePath = environment.api_url + 'contacts/';

    constructor(
        private http: HttpClient
    ) {
    }

    getPendingCountForUser(userId: number): Observable<number> {
        const path = this.basePath + 'pending-count/' + userId;

        return this.http.get(path).pipe(
            map((response: any) => response.data)
        );
    }

    getPendingForUser(userId: number): Observable<Contact[]> {
        const path = this.basePath + 'pending/' + userId;

        return this.http.get(path).pipe(
            map((response: any) => response.data)
        );
    }

    addContact(contactId: number): Observable<Contact> {
        const path = this.basePath + 'create/' + contactId;

        return this.http.post(path, {}).pipe(
            map((response: any) => response.data)
        );
    }

    removeContact(contactId: number): Observable<any> {
        const path = this.basePath + 'remove/' + contactId;

        return this.http.delete(path).pipe(
            map((response: any) => response.data)
        );
    }

    getForUser(userId: number): Observable<User[]> {
        const path = this.basePath + 'user/' + userId;

        return this.http.get(path).pipe(
            map((response: any) => response.data)
        );
    }

    status(userAId: number, userBId: number): Observable<{ status: ContactStatusEnum }> {
        const path = this.basePath + 'status';

        const payload = {
            user_a: userAId,
            user_b: userBId
        };

        return this.http.post(path, payload).pipe(
            map(response => (<any>response).data)
        );
    }
}
