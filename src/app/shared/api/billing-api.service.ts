import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Order} from "../models/Order";
import {map} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})
export class BillingApiService {

    readonly basePath = environment.api_url + 'billing/';

    constructor(
        private http: HttpClient
    ) {
    }

    createOrder(): Observable<string> {
        const path = this.basePath + 'create';

        return this.http.post(path, {}).pipe(
            map((response: any) => response)
        );
    }

    captureOrder(): Observable<Order> {
        const path = this.basePath + 'capture';

        return this.http.post(path, {}).pipe(
            map((response: any) => response)
        );
    }

    cancelOrder(): Observable<any> {
        const path = this.basePath + 'cancel';

        return this.http.post(path, {}).pipe(
            map((response: any) => response)
        );
    }
}
