import {HttpClient, HttpParams} from "@angular/common/http";
import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {map} from "rxjs/operators";
import {environment} from "../../../environments/environment";
import {CreatePostPayload, IndexPostsPayload, Post} from "../models/Post";

@Injectable({
    providedIn: 'root'
})
export class PostsApiService {

    private basePath = `${environment.api_url}posts/`;

    constructor(
        private http: HttpClient
    ) {
    }

    index(payload: IndexPostsPayload): Observable<Post[]> {
        const path = this.basePath + 'index';

        const httpParams = new HttpParams()
            .set('page', payload.page)
            .set('page_size', payload.page_size);

        return this.http.get(path, {params: httpParams}).pipe(
            map((response: any) => response.data)
        );
    }

    getForUser(userId: number, payload: IndexPostsPayload): Observable<Post[]> {
        const path = this.basePath + 'user/' + userId;

        const httpParams = new HttpParams()
            .set('page', payload.page)
            .set('page_size', payload.page_size);

        return this.http.get(path, {params: httpParams}).pipe(
            map((response: any) => response.data)
        );
    }

    getForGroup(groupid: number, payload: any): Observable<Post[]> {
        const path = this.basePath + 'group/' + groupid;

        const httpParams = new HttpParams()
            .set('page', payload.page)
            .set('page_size', payload.page_size);

        return this.http.get(path, {params: httpParams}).pipe(
            map((response: any) => response.data)
        );
    }

    create(payload: CreatePostPayload): Observable<any> {
        const path = this.basePath + 'create';

        const formData = new FormData();
        formData.append('text', payload.text.trim());
        formData.append('privacy', payload.privacy);

        if (payload.tags) {
            formData.append('tags', payload.tags.trim());
        }

        if (payload.group_id) {
            formData.append('group_id', payload.group_id.toString());
        }

        if (payload.attachments) {
            payload.attachments.forEach(
                value => formData.append('attachment_' + payload.attachments.indexOf(value), value, value.name)
            )
        }

        return this.http.post<any>(path, formData, {reportProgress: true, observe: 'events'});
    }

    delete(postId: number): Observable<boolean> {
        const path = this.basePath + postId;

        return this.http.delete(path).pipe(
            map((response: any) => (response as boolean))
        )
    }

    like(postId: number): Observable<any> {
        const path = this.basePath + 'like/' + postId;

        return this.http.post(path, {}).pipe(
            map((response: any) => response.data)
        )
    }

    unlike(postId: number): Observable<any> {
        const path = this.basePath + 'unlike/' + postId;

        return this.http.delete(path).pipe(
            map((response: any) => response.data)
        )
    }
}
