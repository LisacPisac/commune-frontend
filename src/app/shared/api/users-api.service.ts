import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {RegistrationRequestPayload, UpdateUserPayload, User} from "../models";
import {map} from "rxjs/operators";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
    providedIn: 'root'
})
export class UsersApiService {

    private basePath = `${environment.api_url}users/`;

    constructor(
        private snackbar: MatSnackBar,
        private http: HttpClient
    ) {
    }

    get(userId: number) {
        const path = this.basePath + userId;
        return this.http.get<User>(path).pipe(
            map(response => (response as any).data)
        );
    }

    getProfile() {
        const path = this.basePath + 'profile';
        return this.http.get<User>(path).pipe(
            map(response => (response as any).data)
        );
    }

    register(payload: RegistrationRequestPayload): Observable<User> {
        const path = this.basePath + 'register';
        return this.http.post<User>(path, payload).pipe(
            map(response => (response as any).data)
        );
    }

    updateProfile(userId: number, payload: UpdateUserPayload): Observable<HttpEvent<any>> {
        const path = this.basePath + 'update/' + userId;

        const formData = new FormData();
        formData.append('email', payload.email);
        formData.append('username', payload.username);
        formData.append('subtitle', payload.subtitle);
        formData.append('summary', payload.summary);

        if (payload.avatar) {
            formData.append('attachment_' + payload.avatar.name, payload.avatar, payload.avatar.name)
        }

        return this.http.post<HttpEvent<any>>(path, formData, {reportProgress: true, observe: 'events'});
    }

    delete(userId: number): Observable<any> {
        const path = this.basePath + 'delete/' + userId;

        return this.http.delete(path).pipe(
            map(response => (response as any).data)
        );
    }
}
