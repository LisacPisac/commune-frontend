import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ScrollingModule} from "@angular/cdk/scrolling";
import {OverlayModule} from "@angular/cdk/overlay";


@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ScrollingModule,
        OverlayModule
    ]
})
export class CdkModule {
}
