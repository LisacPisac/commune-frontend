import {RouterModule} from '@angular/router';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "./material/material.module";
import {ReactiveFormsModule} from "@angular/forms";
import {AuthService, AuthUserService, ErrorService} from "./services";
import {PostCardComponent} from './components/post-card/post-card.component';
import {FlexLayoutModule} from "@angular/flex-layout";
import {CreatePostComponent} from './components/create-post/create-post.component';
import {
    CreatePostDialogComponent
} from './components/dialogs/create-group-dialog/create-post-dialog/create-post-dialog.component';
import {ProfileCardComponent} from './components/profile-card/profile-card.component';
import {FileUploadComponent} from './components/file-upload/file-upload.component';
import {ImageDialogComponent} from './components/image-dialog/image-dialog.component';
import {VimeModule} from "@vime/angular";
import {CommentSectionComponent} from './components/post-card/comment-section/comment-section.component';
import {CommentComponent} from './components/post-card/comment-section/comment/comment.component';
import {CommentDialogComponent} from './components/dialogs/add-comment-dialog/comment-dialog.component';
import {CommentThreadComponent} from './components/post-card/comment-section/comment-thread/comment-thread.component';
import {NgxScrollEventModule} from "ngx-scroll-event";
import {
    PendingRequestsDialogComponent
} from './components/dialogs/pending-requests-dialog/pending-requests-dialog.component';
import {CreateGroupDialogComponent} from './components/dialogs/create-group-dialog/create-group-dialog.component';
import {
    PendingContactsDialogComponent
} from './components/dialogs/pending-contacts-dialog/pending-contacts-dialog.component';
import {GroupCardComponent} from "./components/group-card/group-card.component";
import {DebounceClickDirective} from './directives/debounce-click.directive';
import {PostListComponent} from './components/post-list/post-list.component';

const COMPONENTS = [
    PostCardComponent,
    GroupCardComponent,
    CreatePostComponent,
    CreatePostDialogComponent,
    ProfileCardComponent,
    FileUploadComponent,
    CreatePostComponent,
    ImageDialogComponent,
    CommentSectionComponent,
    CommentComponent,
    CommentDialogComponent,
    CommentThreadComponent,
    PostListComponent
];

const DIRECTIVES = [
    DebounceClickDirective
];

const DIALOGS = [
    PendingRequestsDialogComponent,
    CreateGroupDialogComponent,
    PendingContactsDialogComponent,
];

@NgModule({
    declarations: [
        ...COMPONENTS,
        ...DIALOGS,
        ...DIRECTIVES,
        PostListComponent,
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule,
        MaterialModule,
        FlexLayoutModule,
        VimeModule,
        NgxScrollEventModule
    ],
    exports: [
        CommonModule,
        ReactiveFormsModule,
        MaterialModule,
        FlexLayoutModule,
        VimeModule,
        NgxScrollEventModule,
        ...COMPONENTS,
        ...DIRECTIVES,
    ],
})
export class SharedModule {
    // constructor(@Optional() @SkipSelf() parentModule?: SharedModule) {
    //     if (parentModule) {
    //         throw new Error(
    //             'SharedModule is already loaded. Import it in the AppModule only');
    //     }
    // }

    static forRoot(): ModuleWithProviders<SharedModule> {
        return {
            ngModule: SharedModule,
            providers: [
                AuthService,
                ErrorService,
                AuthUserService,
            ]
        };
    }
}
