import {AuthUserService} from './../services/auth-user.service';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from '../models/User';
import {UsersApiService} from './../api/users-api.service';

@Injectable({
    providedIn: 'root'
})
export class UserResolver implements Resolve<User> {

    constructor(
        private usersApi: UsersApiService,
        private authUser: AuthUserService,
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> {
        return this.usersApi.get(parseInt(route.paramMap.get('userId')));
    }
}
