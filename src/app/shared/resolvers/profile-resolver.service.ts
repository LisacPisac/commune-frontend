import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {User} from "../models";
import {UsersApiService} from "../api";
import {AuthUserService} from "../services";

@Injectable({
    providedIn: 'root'
})
export class ProfileResolver implements Resolve<User> {
    constructor(
        private userApi: UsersApiService,
        private authUser: AuthUserService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User> | Promise<User> | Observable<never> {
        return this.userApi.getProfile().toPromise().then(
            (result) => {
                this.authUser.user = result;
                return result;
            }
        )
    }
}
