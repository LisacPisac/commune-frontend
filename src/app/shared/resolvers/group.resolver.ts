import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {GroupsApiService} from '../api/groups-api.service';
import {Group} from '../models/Group';

@Injectable({
    providedIn: 'root'
})
export class GroupResolver implements Resolve<Group> {

    constructor(
        private groupsApi: GroupsApiService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Group> {
        return this.groupsApi.get(parseInt(route.paramMap.get('groupId')));
    }
}
