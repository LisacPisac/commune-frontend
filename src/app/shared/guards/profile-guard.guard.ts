import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthUserService} from './../services/auth-user.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileGuard implements CanActivate {

    constructor(
        private authUser: AuthUserService,
        private router: Router
    ) {
    }

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

        if (route.paramMap.get('userId')) {
            if (this.authUser?.user?.id == parseInt(route.paramMap.get('userId'))) {
                this.router.navigate(['profile']);
                return false;
            }
        }

        return true;
    }
}
