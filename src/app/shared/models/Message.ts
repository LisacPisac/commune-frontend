import {User} from "./User";
import {Chat} from "./Chat";

export type Message = {
    id: number;
    text: string;
    read: boolean;
    chat: Chat;
    user: User;
    created_at: string;
    updated_at: string;
}

export interface LoadMessagesRequest {
    page: number;
    chat_id: number;
}
