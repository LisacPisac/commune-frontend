import {User} from "./User";

export enum ContactStatusEnum {
    NOT_CONTACTS,
    PENDING,
    CONTACTS
}

export type Contact = {
    user_a: User,
    user_b: User,
    pending: boolean;
}
