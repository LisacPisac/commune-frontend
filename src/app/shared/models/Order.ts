import {User} from "./User";

export type Order = {
    id: number;
    user: User;
    status: string;
    created_at: string;
    updated_at: string;
}
