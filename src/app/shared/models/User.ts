import {Group} from "./Group"

export type RegistrationRequestPayload = {
    email: string;
    username: string;
    password: string;
}

export type User = {
    id: number;
    email: string;
    username: string;
    enabled: boolean;
    avatar: string;
    summary: string;
    subtitle: string;
    groups: Group[];
    tfa_enabled: boolean;
    is_premium: boolean;
    is_deleted: boolean;
}

export type UserWithToken = {
    id: number;
    email: string;
    username: string;
    enabled: boolean;
    token: string;
}

export type UpdateUserPayload = {
    email: string;
    username: string;
    avatar?: File;
    summary?: string;
    subtitle?: string;
}
