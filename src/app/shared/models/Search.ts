import {Group} from "./Group";
import {User} from "./User";
import {Post} from "./Post";

export type SearchResult = {
    users: User[];
    groups: Group[];
    posts: Post[];
}
