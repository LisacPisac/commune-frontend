import {User} from "./User";

export type Chat = {
    id: number;
    users: User[];
    name?: string;
    created_at: string;
    updated_at: string;
}

export interface SendMessagePayload {
    chat_id: number;
    text: string;
}
