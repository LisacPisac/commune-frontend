export type ErrorResponse = {
    errors: Error[];
}

export type Error = {
    field: string;
    message: string;
    type: string;
    code: number;
    metaData: any[];
}
