export interface LoginRequestPayload {
    email: string;
    password: string;
    tfa_code?: string;
}

export interface TfaSecret {
    secret: string;
    qr_code: string;
    qr_text: string;
}

export interface ActivateTfaPayload {
    code: string;
}
