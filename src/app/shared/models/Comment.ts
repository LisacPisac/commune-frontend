import {User} from "./User";

export interface GetCommentRequestPayload {
    post_id: number;
    page: number;
    comment_id: number; // Essentially thread ID
}

export interface CreateCommentRequestPayload {
    post_id: number;
    text: string;
}

export interface EditCommentRequestPayload {
    comment_id: number;
    text: string;
}

export interface ReplyCommentRequestPayload {
    reply_comment_id: number;
    text: string;
}

export type Comment = {
    id: number;
    post_id: number
    comment_id?: number;
    text: string;
    author: User;
    created_at: string;
    updated_at: string;
    is_deleted: boolean;
    replies: Comment[];
}
