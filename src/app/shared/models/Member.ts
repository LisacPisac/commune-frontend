import {Group} from "./Group";
import {User} from "./User";

export interface UpdateMembershipPayload {
    user_id: number;
    group_id: number;
    pending?: number;
    admin?: number;
}

export interface DeleteMemberPayload {
    user_id: number;
    group_id: number;
}

export type PendingMemberCount = {
    count: number;
}

export type Member = {
    user: User;
    group: Group;
    admin: boolean;
    pending: boolean;
}
