import {File as DbFile} from "./File";
import {User} from "./User";

export interface IndexGroupsPayload {
    page: number;
    limit: number;
}

export interface CreateGroupPayload {
    name: string;
}

export interface UpdateGroupPayload {
    name: string;
    avatar?: File;
    cover?: File;
}

export interface JoinGroupPayload {
    user_id: number;
}

export interface GetMembershipStatusPayload {
    user_id: number;
}

export type Group = {
    id: number;
    name: string;
    owner: User;
    avatar_file: DbFile;
    cover_file: DbFile;
    created_at: string;
    updated_at: string;
}
