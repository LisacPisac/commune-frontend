import {User} from "./User";
import {File as UserFile} from "./File";
import {Comment} from "./Comment";
import {Like} from "./Like";
import {Group} from "./Group";

export const postTypes = {
    text: {
        id: "1",
        identifier: 'text'
    },
    audio: {
        id: "2",
        identifier: 'audio'
    },
    video: {
        id: "3",
        identifier: 'video'
    },
}

export type PostType = {
    id: number,
    identifier: string,
}

export interface CreatePostPayload {
    text: string;
    attachments?: File[];
    tags?: string;
    group_id?: number;
    privacy: string;
}

export interface IndexPostsPayload {
    page: number;
    page_size: number;
}

export type Post = {
    id: number;
    text: number;
    tags: string[];
    author: User;
    group?: Group;
    files: UserFile[];
    comments?: Comment[];
    comments_count: number;
    created_at: string;
    likes: Like[];
    likes_count: number;
}
