export type File = {
    id: number;
    file: string; // base64
    type: string; // mime-type
    created_at: string;
}
