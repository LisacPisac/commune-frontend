import {Component, Inject, OnInit} from '@angular/core';
import {File} from "../../models";
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import {SafeResourceUrl} from "@angular/platform-browser";
import {environment} from "../../../../environments/environment";

@Component({
    selector: 'app-image-dialog',
    templateUrl: './image-dialog.component.html',
    styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent implements OnInit {

    imageSrc: SafeResourceUrl;

    mediaUrl = environment.media_url;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: { file: File },
    ) {
    }

    ngOnInit(): void {
        this.imageSrc = this.mediaUrl + this.data.file;
    }
}
