import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {finalize} from "rxjs/operators";
import {HttpErrorResponse, HttpEventType} from "@angular/common/http";
import {Observable, Subscription} from "rxjs";
import {FormControl} from "@angular/forms";


@Component({
    selector: 'app-file-upload',
    templateUrl: './file-upload.component.html',
    styleUrls: ['./file-upload.component.scss']
})
export class FileUploadComponent implements OnInit, OnDestroy {

    @Input() label: string = 'Upload file';
    @Input() formControl: FormControl;
    @Input() $apiCall: Observable<any>;

    @Output() uploaded: EventEmitter<boolean> = new EventEmitter<boolean>();

    uploadProgress: number = 0;

    private subscription: Subscription;

    constructor() {
    }

    ngOnInit(): void {

    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    handle() {
        this.subscription = this.$apiCall.pipe(
            finalize(() => this.reset())
        ).subscribe(
            (event) => {
                if (event.type == HttpEventType.UploadProgress) {
                    this.uploadProgress = Math.round(100 * (event.loaded / event.total));
                }
            },
            (error: HttpErrorResponse) => {
                this.uploaded.emit(false);
            }
        );
    }

    private reset() {
        this.uploaded.emit(true);
        this.uploadProgress = null;
        this.subscription = null;
    }
}
