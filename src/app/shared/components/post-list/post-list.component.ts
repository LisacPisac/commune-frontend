import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Post} from "../../models";

@Component({
    selector: 'app-post-list',
    templateUrl: './post-list.component.html',
    styleUrls: ['./post-list.component.scss']
})
export class PostListComponent {
    @Input() posts: Post[];
    @Input() hideGroup = false;

    @Output() onPage: EventEmitter<any> = new EventEmitter<any>();

    onLoad() {
        this.onPage.emit();
    }

    onDelete(post: Post) {
        if (post) {
            this.posts = this.posts.filter(
                (postItem: Post) => {
                    return postItem.id !== post.id;
                }
            );
        }
    }
}
