import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Group} from 'src/app/shared/models/Group';
import {environment} from 'src/environments/environment';

@Component({
    selector: 'app-group-card',
    templateUrl: './group-card.component.html',
    styleUrls: ['./group-card.component.scss']
})
export class GroupCardComponent implements OnInit {

    @Input() group: Group;

    mediaUrl = environment.media_url;

    constructor(
        private router: Router
    ) { }

    ngOnInit(): void {
    }

    generateGroupRoute() {
        return this.router.navigate
    }
}
