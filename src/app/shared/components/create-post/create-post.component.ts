import {Group, Post} from '../../models';
import {Component, EventEmitter, Input, OnDestroy, Output} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from "rxjs";
import {PostsApiService} from "../../api";
import {AuthUserService} from "../../services";
import {
    CreatePostDialogComponent
} from "../dialogs/create-group-dialog/create-post-dialog/create-post-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-create-post',
    templateUrl: './create-post.component.html',
    styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnDestroy {

    @Input() group: Group;

    @Output() post = new EventEmitter<Post>();

    private subscription: Subscription;

    constructor(
        private authService: AuthUserService,
        private postApi: PostsApiService,
        private dialog: MatDialog,
        private snackBar: MatSnackBar,
    ) {
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    openPostDialog() {
        const dialogData = {
            group: this.group ?? null
        };

        const dialogRef = this.dialog.open(CreatePostDialogComponent, {maxHeight: 800, data: dialogData});

        this.subscription = dialogRef.afterClosed().subscribe(
            (result?: Post) => {
                if (result) {
                    this.snackBar.open('Post created successfully', 'X', {duration: 4000})
                }
                this.post.emit(result);
            }
        );
    }
}
