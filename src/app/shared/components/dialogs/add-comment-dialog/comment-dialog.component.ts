import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {AuthUserService, ErrorService} from "../../../services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {
    Comment,
    CreateCommentRequestPayload,
    EditCommentRequestPayload,
    Post,
    ReplyCommentRequestPayload,
    User
} from "../../../models";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CommentsApiService} from "../../../api";
import {debounceTime} from "rxjs/operators";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-comment-dialog',
    templateUrl: './comment-dialog.component.html',
    styleUrls: ['./comment-dialog.component.scss']
})
export class CommentDialogComponent implements OnInit, OnDestroy {

    form!: FormGroup;
    user!: User;
    isEditing = false;
    isReplying = false;

    private subscription: Subscription;

    constructor(
        public dialogRef: MatDialogRef<CommentDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { post: Post, comment?: Comment, replyComment?: Comment },
        private authUser: AuthUserService,
        private snackbar: MatSnackBar,
        private formBuilder: FormBuilder,
        private commentApi: CommentsApiService,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group(
            {text: ['', [Validators.required]]}
        );

        if (this.data.comment) {
            this.isEditing = true;
            this.form.get('text').patchValue(this.data.comment.text);
        }

        if (this.data.replyComment) {
            this.isReplying = true;
        }
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    onSubmit() {
        if (!this.isEditing && !this.isReplying) {
            const payload: CreateCommentRequestPayload = {
                post_id: this.data.post.id,
                text: this.form.get('text').value
            }

            this.subscription = this.commentApi.create(payload)
                .pipe(debounceTime(500))
                .subscribe(
                    (result: Comment) => {
                        this.dialogRef.close(result);
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open(this.errorService.handleErrors(error));
                    }
                );
        }

        if (this.isEditing) {
            const payload: EditCommentRequestPayload = {
                comment_id: this.data.comment.id,
                text: this.form.get('text').value
            }

            this.subscription = this.commentApi.edit(this.data.comment.id, payload)
                .pipe(debounceTime(500))
                .subscribe(
                    (result: Comment) => {
                        this.dialogRef.close(result);
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open(this.errorService.handleErrors(error));
                    }
                );
        }

        if (this.isReplying) {
            const payload: ReplyCommentRequestPayload = {
                reply_comment_id: this.data.replyComment.id,
                text: this.form.get('text').value
            }

            this.subscription = this.commentApi.reply(payload)
                .pipe(debounceTime(500))
                .subscribe(
                    (result: Comment) => {
                        this.dialogRef.close(result);
                    },
                    (error: HttpErrorResponse) => {
                        this.snackBar.open(this.errorService.handleErrors(error));
                    }
                );
        }
    }

    onClose() {
        this.dialogRef.close();
    }
}
