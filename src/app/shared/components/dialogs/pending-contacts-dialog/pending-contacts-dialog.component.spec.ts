import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PendingContactsDialogComponent} from './pending-contacts-dialog.component';

describe('PendingContactsDialogComponent', () => {
  let component: PendingContactsDialogComponent;
  let fixture: ComponentFixture<PendingContactsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingContactsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingContactsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
