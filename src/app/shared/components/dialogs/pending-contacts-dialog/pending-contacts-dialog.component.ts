import {Component, OnInit} from '@angular/core';
import {Contact} from "../../../models";
import {environment} from "../../../../../environments/environment";
import {Subscription} from "rxjs";
import {MatDialogRef} from "@angular/material/dialog";
import {ContactsApiService} from "../../../api";
import {HttpErrorResponse} from "@angular/common/http";
import {AuthUserService, ErrorService} from "../../../services";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-pending-contacts-dialog',
    templateUrl: './pending-contacts-dialog.component.html',
    styleUrls: ['./pending-contacts-dialog.component.scss']
})
export class PendingContactsDialogComponent implements OnInit {

    requests: Contact[];

    mediaUrl = environment.media_url;
    loading: boolean;

    private subscription = new Subscription();

    constructor(
        private authUser: AuthUserService,
        private dialogRef: MatDialogRef<PendingContactsDialogComponent>,
        private contactApi: ContactsApiService,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.loadData();
    }

    loadData() {
        this.loading = true;
        this.subscription.add(
            this.contactApi.getPendingForUser(this.authUser.userId).subscribe(
                (result) => {
                    this.requests = result;
                    this.loading = false;
                },
                (error: HttpErrorResponse) => {
                    this.loading = true;
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            )
        )
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    accept(request: Contact) {
        this.subscription.add(
            this.contactApi.addContact(this.getUserRequestId(request)).subscribe(
                (result) => {
                    if (result.user_a.id !== this.authUser.userId) {
                        this.snackBar.open(result.user_a.username + '\'s added to contacts', 'X');
                        this.loadData();
                    }

                    if (result.user_b.id !== this.authUser.userId) {
                        this.snackBar.open(result.user_b.username + '\'s added to contacts', 'X');
                        this.loadData();
                    }
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            )
        );
    }

    decline(request: Contact) {
        this.subscription.add(
            this.contactApi.removeContact(this.getUserRequestId(request)).subscribe(
                (result) => {
                    if (request.user_a.id !== this.authUser.userId) {
                        this.snackBar.open(request.user_a.username + '\'s contact request has been declined', 'X');
                        this.loadData();
                    }

                    if (request.user_b.id !== this.authUser.userId) {
                        this.snackBar.open(request.user_b.username + '\'s contact request has been declined', 'X');
                        this.loadData();
                    }
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            )
        );
    }

    getUserRequestAvatar(request: Contact): string {
        if (request.user_a.id !== this.authUser.userId) {
            if (request.user_a.avatar) {
                return this.mediaUrl + request.user_a.avatar;
            }
        } else {
            if (request.user_b.avatar) {
                return this.mediaUrl + request.user_b.avatar;
            }
        }
        return 'assets/resources/img/default_avatar_48dp.svg';
    }

    getUserRequestId(request: Contact): number {
        if (request.user_a.id !== this.authUser.userId) {
            return request.user_a.id;
        } else {
            return request.user_b.id;
        }
    }

    getUserRequestUsername(request: Contact): string {
        if (request.user_a.id !== this.authUser.userId) {
            return request.user_a.username;

        } else {
            return request.user_b.username;
        }
    }
}
