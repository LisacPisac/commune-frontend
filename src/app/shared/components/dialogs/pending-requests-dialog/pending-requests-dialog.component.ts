import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {MembersApiService} from "../../../api";
import {Member, UpdateMembershipPayload} from "../../../models";
import {environment} from "../../../../../environments/environment";
import {Subscription} from "rxjs";
import {HttpErrorResponse} from "@angular/common/http";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ErrorService} from "../../../services";

@Component({
    selector: 'app-pending-requests-dialog',
    templateUrl: './pending-requests-dialog.component.html',
    styleUrls: ['./pending-requests-dialog.component.scss']
})
export class PendingRequestsDialogComponent implements OnInit, OnDestroy {

    requests: Member[];

    mediaUrl = environment.media_url;

    private subscription: Subscription;

    constructor(
        private dialogRef: MatDialogRef<PendingRequestsDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { requests: Member[] },
        private membersApi: MembersApiService,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.requests = this.data.requests;
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    approve(request: Member) {
        const payload: UpdateMembershipPayload = {
            user_id: request.user.id,
            group_id: request.group.id,
            pending: 0,
            admin: 0
        };
        this.subscription = this.membersApi.update(payload).subscribe(
            (result) => {
                this.requests = this.requests.filter((req) => req.user.id !== request.user.id);
                this.snackBar.open(request.user.username + '\'s request has been approved', 'X');
            },
            (error: HttpErrorResponse) => {
                this.snackBar.open(this.errorService.handleErrors(error), 'X');
            }
        );
    }

    decline(request: Member) {
        this.subscription = this.membersApi.delete(request.user.id, request.group.id).subscribe(
            (result) => {
                this.requests = this.requests.filter((req) => req.user.id !== request.user.id);
                this.snackBar.open(request.user.username + '\'s request has been approved', 'X');
            },
            (error: HttpErrorResponse) => {
                this.snackBar.open(this.errorService.handleErrors(error));
            }
        )
    }
}
