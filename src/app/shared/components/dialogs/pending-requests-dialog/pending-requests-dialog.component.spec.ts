import {ComponentFixture, TestBed} from '@angular/core/testing';

import {PendingRequestsDialogComponent} from './pending-requests-dialog.component';

describe('PendingRequestsDialogComponent', () => {
  let component: PendingRequestsDialogComponent;
  let fixture: ComponentFixture<PendingRequestsDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PendingRequestsDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingRequestsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
