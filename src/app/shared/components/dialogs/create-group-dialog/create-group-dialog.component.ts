import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Subscription} from "rxjs";
import {CreateGroupPayload} from "../../../models";
import {MatDialogRef} from "@angular/material/dialog";
import {AuthUserService} from "../../../services/auth-user.service";
import {GroupsApiService} from "../../../api";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-create-group-dialog',
    templateUrl: './create-group-dialog.component.html',
    styleUrls: ['./create-group-dialog.component.scss']
})
export class CreateGroupDialogComponent implements OnInit {

    readonly imageRegex = /image/g;
    readonly audioRegex = /audio/g;
    readonly videoRegex = /video/g;
    readonly documentRegex = /doc|docx|xml|application\/msword|application\/vnd.openxmlformats-officedocument.wordprocessingml.document/g;

    form!: FormGroup;

    private subscriptions = new Subscription();

    constructor(
        private dialogref: MatDialogRef<CreateGroupDialogComponent>,
        private formBuilder: FormBuilder,
        private authService: AuthUserService,
        private groupApi: GroupsApiService,
    ) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            name: [null, [Validators.required]],
        });
    }

    ngOnDestroy(): void {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    onSubmit() {
        if (this.form.valid) {
            const payload: CreateGroupPayload = {
                name: this.form.get('name').value
            };

            this.subscriptions = this.groupApi.create(payload)
                .subscribe(
                    (result) => {
                        this.dialogref.close(result);
                    },
                    (error: HttpErrorResponse) => {

                    },
                );
        }
    }
}
