import {HttpErrorResponse, HttpEventType} from "@angular/common/http";
import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Subscription} from "rxjs";
import {PostsApiService} from "../../../../api";
import {CreatePostPayload, Group} from '../../../../models';
import {AuthUserService, ErrorService} from "../../../../services";
import {postTagsValidator} from "../../../../validators/postTagsValidator";

@Component({
    selector: 'app-create-post-dialog',
    templateUrl: './create-post-dialog.component.html',
    styleUrls: ['./create-post-dialog.component.scss']
})
export class CreatePostDialogComponent implements OnInit, OnDestroy {

    form!: FormGroup;

    uploadProgress: number = 0;

    files: File[] = [];

    private subscriptions = new Subscription();
    private uploadSub: Subscription;
    private group: Group;

    constructor(
        private dialogref: MatDialogRef<CreatePostDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: { group?: Group },
        private formBuilder: FormBuilder,
        private authService: AuthUserService,
        private postApi: PostsApiService,
        private errorService: ErrorService
    ) {
    }

    ngOnInit(): void {
        this.form = this.formBuilder.group({
            text: [''],
            files: [null],
            tags: ['', [postTagsValidator]],
            privacy: ['everyone']
        });

        this.group = this.data.group;

        this.subscriptions.add(
            this.form.get('files').valueChanges.subscribe(
                (files) => {
                    this.files = files;
                }
            )
        )
    }

    ngOnDestroy(): void {
        if (this.subscriptions) {
            this.subscriptions.unsubscribe();
        }
    }

    onSubmit() {
        if (this.form.valid) {
            this.form.markAsTouched();
            const payload: CreatePostPayload = {
                attachments: this.form.get('files').value,
                tags: this.form.get('tags').value,
                text: this.form.get('text').value,
                privacy: this.form.get('privacy').value,
            };

            if (this.group) {
                payload.group_id = this.group.id;
            }

            this.subscriptions = this.postApi.create(payload)
                .subscribe(
                    (event) => {
                        if (event.type == HttpEventType.UploadProgress) {
                            this.uploadProgress = Math.round(100 * (event.loaded / event.total));
                        }

                        if (event.type == HttpEventType.Response) {
                            this.reset();
                            this.dialogref.close(event.body.data);
                        }
                    },
                    (error: HttpErrorResponse) => {
                        this.reset();
                        this.errorService.handleErrors(error, this.form);
                    },
                    () => {
                        this.reset();
                    }
                );
        }
    }

    private reset() {
        this.uploadProgress = null;
        this.uploadSub = null;
    }

    resolveFileIcon(file: File) {
        const imageRegex = /image/g;
        const audioRegex = /audio/g;
        const videoRegex = /video/g;
        const documentRegex = /doc|docx|xml|pdf|application\/msword|application\/vnd.openxmlformats-officedocument.wordprocessingml.document/g;

        if (imageRegex.test(file.type)) {
            return 'image';
        }

        if (audioRegex.test(file.type)) {
            return 'headphones';
        }

        if (videoRegex.test(file.type)) {
            return 'movie';
        }

        if (documentRegex.test(file.type)) {
            return 'description';
        }

        return 'attach_file';
    }

    isGroupPost(): boolean {
        return !!this.group;
    }

    clearFiles() {
        this.files = [];
        this.form.get('files').setValue(null);
    }
}
