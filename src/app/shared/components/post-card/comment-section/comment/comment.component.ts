import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy} from '@angular/core';
import {environment} from "../../../../../../environments/environment";
import {Comment} from "../../../../models";
import {CommentDialogComponent} from "../../../dialogs/add-comment-dialog/comment-dialog.component";
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {ErrorService} from "../../../../services";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CommentComponent implements OnDestroy {

    @Input() comment: Comment;

    mediaUrl = environment.media_url;

    private subscription: Subscription;

    constructor(
        private dialog: MatDialog,
        private changeDetectorRef: ChangeDetectorRef,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    reply() {
        const commentDialogRef = this.dialog.open(CommentDialogComponent, {data: {replyComment: this.comment}});

        this.subscription = commentDialogRef.afterClosed().subscribe(
            (result: Comment) => {
                if (result) {
                    this.comment.replies.push(result);
                    this.changeDetectorRef.markForCheck();
                }
            },
            (error: HttpErrorResponse) => {
                this.snackBar.open(this.errorService.handleErrors(error), 'X');
            }
        );
    }
}
