import {Component, Input, OnInit} from '@angular/core';
import {Comment} from "../../../../models/Comment";

@Component({
    selector: 'app-comment-thread',
    templateUrl: './comment-thread.component.html',
    styleUrls: ['./comment-thread.component.scss']
})
export class CommentThreadComponent implements OnInit {

    @Input() comments: Comment[];

    constructor() {
    }

    ngOnInit(): void {
    }

}
