import {HttpErrorResponse} from '@angular/common/http';
import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    EventEmitter,
    Input,
    OnDestroy,
    OnInit,
    Output
} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {Subscription} from 'rxjs';
import {environment} from "../../../../environments/environment";
import {PostsApiService} from '../../api';
import {Comment, File, Like, Post} from "../../models";
import {AuthUserService, ErrorService} from '../../services';
import {CommentDialogComponent} from "../dialogs/add-comment-dialog/comment-dialog.component";
import {ImageDialogComponent} from "../image-dialog/image-dialog.component";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
    selector: 'app-post',
    templateUrl: './post-card.component.html',
    styleUrls: ['./post-card.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostCardComponent implements OnInit, OnDestroy {

    @Input() post: Post;
    @Input() hideGroup = false;

    @Output() onDelete = new EventEmitter<Post>();

    mediaUrl = environment.media_url;

    visibleIndex = 0; // for post attachments

    likeCount = 0;

    private subscription: Subscription;

    constructor(
        private dialog: MatDialog,
        private changeDetectorRef: ChangeDetectorRef,
        private postApi: PostsApiService,
        private authUser: AuthUserService,
        private snackBar: MatSnackBar,
        private errorService: ErrorService,
    ) {
    }

    ngOnInit(): void {
        this.likeCount = this.post.likes_count;
    }

    ngOnDestroy(): void {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }

    openImage(file: File) {
        const imageDialogRef = this.dialog.open(
            ImageDialogComponent,
            {
                data: file,
                height: "850px",
            }
        );
    }

    hasFiles(): boolean {
        return this.post.files.length > 0;
    }

    isImage(file: File): boolean {
        if (!file) {
            return false;
        }

        const imageRegex = /image/g;

        return imageRegex.test(file.type);
    }

    isAudio(file: File): boolean {
        if (!file) {
            return false;
        }

        const audioRegex = /audio/g;

        return audioRegex.test(file.type);
    }

    isVideo(file: File): boolean {
        if (!file) {
            return false;
        }

        const videoRegex = /video/g;

        return videoRegex.test(file.type);
    }

    prevMedia() {
        if ((this.visibleIndex - 1) >= 0) {
            this.visibleIndex--;
            this.changeDetectorRef.markForCheck();
        }
    }

    nextMedia() {
        if ((this.visibleIndex + 1) < this.post.files.length) {
            this.visibleIndex++;
            this.changeDetectorRef.markForCheck();
        }
    }

    addComment() {
        const commentDialogRef = this.dialog.open(CommentDialogComponent, {data: {post: this.post}});

        this.subscription = commentDialogRef.afterClosed().subscribe(
            (result: Comment) => {
                if (result) {
                    this.post.comments.push(result);
                    this.post.comments_count++;
                    this.changeDetectorRef.markForCheck();
                }
            }
        );
    }

    like() {
        this.subscription =
            this.postApi.like(this.post.id).subscribe(
                (result: Like) => {
                    this.likeCount++;
                    this.post.likes.push(result);
                    this.changeDetectorRef.markForCheck();
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            );
    }

    unlike() {
        this.subscription =
            this.postApi.unlike(this.post.id).subscribe(
                (result) => {
                    this.likeCount--;
                    this.post.likes = this.post.likes.filter((like) => (like.user_id !== this.authUser.userId));
                    this.changeDetectorRef.markForCheck();
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            );
    }

    isLikedByUser() {
        let isLiked = false;
        if (this.likeCount <= 0) {
            return false;
        }

        this.post.likes.forEach(
            (like: Like) => {
                if (like.user_id == this.authUser.userId) {
                    isLiked = true;
                }
            }
        );

        return isLiked;
    }

    deletePost() {
        this.subscription =
            this.postApi.delete(this.post.id).subscribe(
                (result: boolean) => {
                    if (result) {
                        this.snackBar.open('Post successfully deleted', 'X', {duration: 4000});
                        this.onDelete.emit(this.post);
                        this.changeDetectorRef.markForCheck();
                    }
                },
                (error: HttpErrorResponse) => {
                    this.snackBar.open(this.errorService.handleErrors(error), 'X');
                }
            );
    }

    isOwnedByUser() {
        return this.post.author.id === this.authUser.user.id;
    }
}
