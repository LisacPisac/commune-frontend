import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

const regExp: RegExp = /[^\w\d#,_]/g;

export function postTagsValidator(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const matches = (control.value as string).match(regExp);
        return (matches.length <= 0) ? {invalidTags: {value: control.value}} : null;
    }
}
