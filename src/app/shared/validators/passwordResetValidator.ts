import {AbstractControl, ValidationErrors, ValidatorFn} from "@angular/forms";

export const passwordResetValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const password = control.get('password');
    const password_repeat = control.get('password_repeat');

    return password && password_repeat
    && password.value && password_repeat.value
    && password.value !== password_repeat.value ? {password_repeat: 'Provided passwords must be identical'} : null;
};
